var script = '/tree/addmaterialrequest/';

var renderSendResultError = function renderSendResultError(error) {
	$('.form__error-notify').html('Ошибка отправки формы.');
}

var renderSendResult = function renderSendResult(result, elem) {
	console.log(result)
	$(elem).closest('.b-block').get(0).outerHTML = result;
};

var prepareValues = function prepareValues (form) {
	var values = {};
	var $form = $(form);
	$.each($form.find('.js-input'), function(i, input) {
		var field = $.trim($(input).attr('name'));
		var value = $.trim($(input).val());
		values[field] = value;
	})
	sendMsg(form, values);
}

var sendMsg = function sendMsg(elem, msg) {
	$.post(script, {action: 'sendMsg', message: msg}, 'json')
		.success(function(reply){renderSendResult(reply, elem)})
		.fail(renderSendResultError);
};

var changeTab = function changeTab(el) {
	
	var $el = $(el);
	var tabNum = $el.attr('href')/1 - 1;
	var $tabs = $el.closest('.tabs').children('.tabs__content').children('.tabs__content__tab')
	$tabs.removeClass('active');   
	console.log($tabs);
	$tabs.eq(tabNum).addClass('active');
	$el.closest('.tabs__nav').find('.tabs__nav__item').removeClass('active');
	$el.addClass('active')
};


$(document).on('click', 'a.tabs__nav__item', function(e){
	e.preventDefault();
	changeTab(this);
})

var validator = $('.js-form').validate({
	submitHandler: function(form) {
		prepareValues(form);
	},
	errorPlacement: function(error, element) {
	    error.appendTo( element.next(".request-form__notify") );
	},
	messages: {
		username: {
			required: 'Заполните поле ФИО',
			maxlength: 'Пожалуйста, введите не более 40 символов',
			pattern: 'Поле ФИО должно содержать имя и фамилию на кириллице, разделенные пробелом'
		},
		usercompany: {
			required: 'Заполните поле компания "Компания"',
			maxlength: 'Пожалуйста, введите не более 40 символов',
			pattern: 'Внимательно заполните поле "Компания"'
		},
		usermail: {
			required: 'Заполните поле "E-mail"',
			maxlength: 'Пожалуйста, введите не более 40 символов',
			pattern: 'Введите корректный адрес эл. почты'
		},
		userphone: {
			required: 'Введите номер телефона"'
		}
	},
	rules: {
		username: {
			required: true,
			maxlength: 40,
			pattern: /^[А-яЁё\s][А-яЁё\-]+\s+[А-яЁё\-]+[А-яЁё\s]$/
		},
		usercompany: {
			required: true,
			maxlength: 40,
			pattern: /^[A-zА-яЁё\-\s0-9]+$/
		},
		usermail: {
			required: true,
			maxlength: 40,
			pattern: /^[A-zА-яЁё][A-zА-яЁё0-9\-\.]{0,}[A-zА-яЁё0-9]@[A-zА-яЁё0-9][A-zА-яЁё0-9\-]{0,}[A-zА-яЁё0-9]{0,}.{1}[A-zА-яЁё0-9]+$/
		},
		userphone: {
			required: true
		}
	}
});

$("#userphone").mask("8(999)999-99-99",{placeholder:"_"});