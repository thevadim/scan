$(window).load(function() {
	// Сохранение формы галереи
	$(document).on('click', 'input#gallery-save, input#gallery-apply', function() {
		var actionType = $(this).attr('name');
		var galleryData = {
			Gallery: {
				actionType: actionType,
				name: $('#Galleries_name').val(),
				id: $('.gallery-form input.Gallery_id').val(),
				photos: []
			}
		};
		$('#XUploadForm-form .template-download, #new-gallery-form .template-download').each(function() {
			var detail_text = '';
			if ($(this).find('textarea.GalleryPhoto_detail-text').attr("id") != undefined)
			{
				detail_text = CKEDITOR.instances[$(this).find('textarea.GalleryPhoto_detail-text').attr("id")].getData();
			}
			else
			{
				detail_text = $(this).find('textarea.GalleryPhoto_detail-text').val();
			}
			galleryData.Gallery.photos.push({
				'id':          $(this).find('input.GalleryPhoto_id').val(),
				'order_num':   $(this).find('input.GalleryPhoto_order_num').val(),
				'filedir':     $(this).find('input.GalleryPhoto_filedir').val(),
				'name':        $(this).find('input.GalleryPhoto_name').val(),
				'alt':         $(this).find('input.GalleryPhoto_alt').val(),
				'copyright':   $(this).find('input.GalleryPhoto_copyright').val(),
				'detail_text': detail_text
			});
		});
		$.ajax({
			type: "POST",
			url: "/admin/gallery/save/",
			data: galleryData,
			dataType: 'json',
			success: function(response) {
				if (response.redirect != '') {
					window.location = response.redirect;
				}
			}
		});
	});


	$("#XUploadForm-form button.btn.btn-warning").live("click", function()
	{
		formValidate.checkForm();
	});

	// Валидация формы
	formValidate = {
		init: function() {
			$('#Galleries_name').blur(function() {
				formValidate.checkForm();
			});
		},
		checkForm: function() {
			var $GalleryName = $('#Galleries_name');
			if ( $GalleryName.val() == '' ) {
				$GalleryName.parent().addClass('error');
				formValidate.disableSubmits();
			} else {
				$GalleryName.parent().removeClass('error');
				formValidate.enableSubmits();
			}
		},
		disableSubmits: function() {
			$('.gallery-form .form-actions input').each(function() {
				$(this).attr('disabled', '1');
			});
		},
		enableSubmits: function() {
			$('.gallery-form .form-actions input').each(function() {
				$(this).removeAttr('disabled');
			});
		}
	};
	formValidate.init();

	// Блокируем сохранение формы пока заливаются файлы
	$(document).on('fileuploadstarted', '#XUploadForm-form',function() {
		uploadsCount = $('#XUploadForm-form .template-upload').length;
		formValidate.disableSubmits();
		completedCount = 0;
	});

	$(document).on('fileuploadcompleted', '#XUploadForm-form', function() {
		completedCount++;
		if (uploadsCount == completedCount) {
			formValidate.checkForm();
		}
	});

	// drag&drop фотографий
	$('.gallery-form table.photos-list tbody').sortable({
		axis: 'y',
		cursor: 'move',
		containment: '.gallery-form',
		start: function(event, ui) {
			wysiwygManager.destroyEditor(ui);

		},
		stop: function(e, ui) {
			$('table.photos-list tr').each(function() {
				$(this).find('input.GalleryPhoto_order_num').val( $(this).index()+1 );
			});
			wysiwygManager.replaceEditor(ui);
		},
		helper: function(e, tr)	{
			var $originals = tr.children();
			var $helper = tr.clone();
			$helper.children().each(function(index)
			{
				$(this).width($originals.eq(index).width())
			});
			return $helper;
		}
	});


	$(".deletephoto").click(function()
	{
		if (confirm('Вы уверены, что хотите удалить фото ' + $(this).parent().parent().find(".GalleryPhoto_name").val() + '?'))
		{
			var obj = this;
			var url = '/admin/gallery/deleteimage/';
			$.get(url, {id:$(this).attr("data")}, function(data) {
				if (data == 'done')
				{
					$(obj).parent().parent().hide();
				}
				else alert(data);
			});
		}
	});

});
