
var codemirror_lang =
{
    button :
    {
        save : 'Сохранить',
        cancel : 'Отмена',
        highlight : 'Подсветка вкл/выкл',
        undo : 'Отменить',
        redo : 'Повторить',
        reindent : 'Выровнять',
        search : 'Поиск'
    },
    searchwins :
    {
        searchfor : 'search term:',
        tryagain : 'search again?',
        endofdoc : 'End of document reached. Start over?'
    }
};
