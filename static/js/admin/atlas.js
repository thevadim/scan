$(function() {
	// Добавить селект со списком рубрик
	$(".add_rubric input").click(function() {
		var select = $(".select_rubric .controls").eq(0).clone();
		select.find("select").removeAttr("disabled").find("option").removeAttr("selected");
		$(".select_rubric").append('<div class="controls">' + select.html() + '</div>');
	});


	$(".add_guide input").click(function() {
		var select = $(".select_guide .controls").eq(0).clone();
		select.find("select").removeAttr("disabled").find("option").removeAttr("selected");
		$(".select_guide").append('<div class="controls">' + select.html() + '</div>');
	});

	// Удалить селект со списком рубрик
	$(".del_rubric").live("click", function() {
		$(this).parent().hide().find("select").attr("disabled", "disabled");
	});


	$('.dot_address').keypress(function(e) {
		if (e.which == 13)
		{
			AtlasGMap.findDot();
		}
	});

	//Карта гидов. При клике на "Найти на карте"
	$('.AtlasGMap_findOnMap').live('click', function() {
		AtlasGMap.findDot();
	});


	// Фото объектов
	$.ajax_upload($('#uploadButton'), {
		action    : '/admin/atlas/objects/uploadimage/',
		name      : 'file[]',
		onSubmit  : function(file, ext) {
			$("#uploadButton").text('Загрузка');
			this.disable();
		},
		onComplete: function(file, response) {
			$("#uploadButton").text('Загрузить');
			this.enable();
			$(".photo").append(response);
		}
	});

	$(".uploadimages span.delete").live("click", function() {
		$(this).parent().parent().remove();
	});
	// Фото объектов
});


//Объект для работы с картой гидов
AtlasGMap = new function() {
	//Объект гугл-карты
	this.map;
	//объект гугл-геодекодера
	this.geocoder;
	//Инициализирована ли карта
	var initialized;

	this.findDot = function()
	{
		var input = $('.dot_address');
		//Получаем адрес из гугла
		geocoder.geocode({ 'address':'Москва, ' + input.val()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK)
			{
				map.setCenter(results[0].geometry.location);
				marker.setPosition(results[0].geometry.location);
				AtlasGMap.updateMarkerCoord(marker);
			}
			else
				alert("Ошибка: " + status);
		});
	}

	this.initialize = function(address, x, y) {
		if (AtlasGMap.initialized == undefined)
		{
			if (!x)
			{
				x = 55.75212;
				y = 37.617665;
			}

			AtlasGMap.addMarker('address', new google.maps.LatLng(x, y));
			geocoder = new google.maps.Geocoder();

			//Центровка карты на Москву
			var latlng = new google.maps.LatLng(x, y);
			var myOptions = {
				scrollwheel:false,
				zoom       :16,
				center     :latlng,
				mapTypeId  :google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		}

		//При инициализации проходим по массиву добавленных маркеров и рисуем их на карте
		AtlasGMap.markers.marker.setMap(map);
		AtlasGMap.initialized = true;
	}


	//Добавление маркера на карту
	this.addMarker = function(title, position) {
		//Точка содержит все параметры и объект googlemap - marker
		var dot = {};
		marker = new google.maps.Marker({
													  position :position,
													  draggable:true
												  });

		//Если инициализация прошла, отображаем на карте сразу
		if (AtlasGMap.initialized)
			marker.setMap(map);

		//При изменении положения необходимо пересчитывать координаты
		google.maps.event.addListener(marker, 'dragend', function() {
			AtlasGMap.updateMarkerCoord(marker)
		});

		dot.marker = marker;
		dot.title = title;

		//Добавляем в массив маркер
		AtlasGMap.markers = dot;
		AtlasGMap.updateMarkerCoord(marker);
	}


	//Обновляет значения координат при перемещении маркера на карте
	this.updateMarkerCoord = function(marker) {
		//Обновляем координаты в инпутах
		$('.dot_x').val(marker.getPosition().lat());
		$('.dot_y').val(marker.getPosition().lng());
	}
}