var codemirror_rootpath = '/static/js/admin/';

CKEDITOR.editorConfig = function( config )
{
	
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.skin = 'v2';
	config.contentsCss = CKEDITOR.basePath + 'content.css';
	// config.enterMode = CKEDITOR.ENTER_BR;
	config.docType = '<!DOCTYPE html>';
	config.autoParagraph = false;
	config.fillEmptyBlocks = false;
	config.enableTabKeyTools = true;
	config.format_tags = 'p;h1;h2;h3';
	
	//чтобы insert вставлял в <p>, а не <div>
	//config.forceEnterMode=true;	
	config.forcePasteAsPlainText = true;
	
	config.extraPlugins = 'onchange,autogrow';
	config.removePlugins = 'resize';
	
	config.autogrow = true;
	config.autoGrow_onStartup = true;
	config.autoGrow_minHeight = 100;

	config.autoGrow_maxHeight = 400;
	config.width = 600;

	
	// config.toolbar_Full =
	// [
	// 	{ name: 'document',    items : [ 'Source'] },
	// 	{ name: 'links',       items : [ 'Link'] },
	// 	{ name: 'insert',      items : [ 'Image'] }
	// ];

	config.toolbar = 
	[
		{ name: 'document',    items : [ 'Source', '-'] },
		{ name: 'clipboard',   items : [ 'PasteText','PasteFromWord','-' ] },
		{ name: 'editing',     items : [ 'Find', 'SelectAll' ] },		
		{ name: 'tools',       items : [ 'Maximize' ] },
		{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight' ] },
		{ name: 'links',       items : [ 'Link','Unlink','Anchor']},
		{ name: 'images',      items : [ 'Image','Flash','Table','HorizontalRule','SpecialChar' ] },	
		{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
		{ name: 'blocks',      items : [ 'ShowBlocks' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline', 'Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'insert',      items : [ 'Styles','Format' , 'Font', 'FontSize'] }
	];



	config.keystrokes =
	[
		[ CKEDITOR.ALT + 121 /*F10*/, 'toolbarFocus' ],
		[ CKEDITOR.ALT + 122 /*F11*/, 'elementsPathFocus' ],
		[ CKEDITOR.SHIFT + 121 /*F10*/, 'contextMenu' ],
		[ CKEDITOR.CTRL + 90 /*Z*/, 'undo' ],
		[ CKEDITOR.CTRL + 89 /*Y*/, 'redo' ],
		[ CKEDITOR.CTRL + CKEDITOR.SHIFT + 90 /*Z*/, 'redo' ],
		[ CKEDITOR.CTRL + 76 /*L*/, 'link' ],
		[ CKEDITOR.CTRL + 66 /*B*/, 'bold' ],
		//[ CKEDITOR.ALT + 109 /*-*/, 'toolbarCollapse' ]
		[ CKEDITOR.CTRL + 73 /*I*/, 'italic' ]
	];
};



CKEDITOR.on( 'instanceReady', function( ev )
{
	var generic = {
		indent : true,
		breakBeforeOpen : true,
		breakAfterOpen : true,
		breakBeforeClose : true,
		breakAfterClose : true
	}
	ev.editor.dataProcessor.writer.setRules( 'p', generic );
	ev.editor.dataProcessor.writer.setRules( 'td', generic );
});
