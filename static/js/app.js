$(document).ready(function() {

	$('[data-svg]').each(function(){
		var s = Snap($(this)[0]);
		Snap.load($(this).attr('data-svg'), function (f) {
		    s.append(f);
		});
	});


	$('[data-image]').each(function() {
		$(this).css('background-image', 'url(' + $(this).attr('data-image') + ')')
	});


	console.log('powered by rubymedia.ru')
});