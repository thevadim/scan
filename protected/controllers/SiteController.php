<?php

class SiteController extends Controller
{
	public $layout = 'inner';

	public function filters()
	{
		return array(
			'accessControl',
			'preaccess',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions'=>array('captcha'), 'users'=>array('*')),
			array('allow', 'actions'=>array('profile'), 'users'=>array('@'),),
			array('deny', 'actions'=>array('profile'), 'users'=>array('*'),),
		);
	}


	/**
	 * Промежуточная авторизация, чтобы сайт был виден из веба, но при этом сам
	 * дизайн не был виден пока не будет пройдена эта авторизация
	 */
	public function actionPreauth()
	{
		$session = Yii::app()->getSession();

		if( isset($session['pre_auth']) )
		{
			$this->redirect('/');
		}

		$login = Yii::app()->request->getParam('login', '');
		$pwd   = Yii::app()->request->getParam('pwd', '');

		if( $login == 'chtpz' && $pwd == 'chtpz' )
		{
			$session = Yii::app()->getSession();

			$session['pre_auth'] = 1;

			$this->redirect('/');
		}

		echo CHtml::form('/site/preauth/');
			echo CHtml::textField('login','', array());
			echo '<br/>';
			echo CHtml::passwordField('pwd','', array());
			echo '<br/>';
			echo CHtml::submitButton('Войти');
		echo CHtml::endForm();
	}


	/**
	 *  Статичные страницы
	 */
	public function actionStatic()
	{
		//Отрезаем слеш слева(у нас в базе без слеша слева лежат урлы по которым будем сравнивать)
		$url = ltrim(Yii::app()->request->requestUri, '/');

		$pockets = array();

		//Вырываем из запроса строку по которой будем искать в базе,
		preg_match('/^(.+?)(\?.*)?$/ui', $url, $pockets);
		//отрезая часть с GET-запросом, которая может быть.
		$url = isset($pockets[1]) ? $pockets[1] : $url;

		//Если в конце нет слеша, то добавляем, так как у нас базе урлы по которым будем искать все лежать со слешем на конце
		$url = $url[mb_strlen($url)-1] !== '/' ? $url . '/' : $url;

		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::CACHE_KEY_TREE_NODE, array(), 'BY_URL_' . $url);

		$node = Yii::app()->cache->get($CACHE_KEY);

		if($node === false)
		{
			$node = Tree::model()->visible()->find('url=:url', array(':url' => $url) );

			if ( empty($node) )
			{
				throw new CHttpException(404);
			}

			Yii::app()->cache->set($CACHE_KEY, $node, 60);

			
		}

		#$this->layout = 'inside';

		/*Уставливаем тайтл, кейвордс и десрипшен для страницы*/
		$this->setSeo(1, array('title' => !empty($node->seo_title) ? $node->seo_title : $node->name));

		if( !empty($node->seo_keywords) )
		{
			$this->pageKeywords = $node->seo_keywords;
		}

		if( !empty($node->seo_description) )
		{
			$this->pageDescription = $node->seo_description;
		}

		Yii::app()->facebook->ogTags['title'] = $this->pageTitle;
		Yii::app()->facebook->ogTags['description'] = $this->pageDescription;

		//Заполняем картинку для тегов фейсбука
		if( !empty($node->preview_img) )
		{
			Yii::app()->facebook->ogTags['image'] = 'http://' . $_SERVER['HTTP_HOST'] . $node->preview_img;
		}

		$this->layout = 'static';

		$this->render('static', array('node' => $node ) );
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'testLimit'=>1,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Рендерим главную страницу
	 */
	public function actionIndex()
	{
    	//Yii::log("^^^^^^^^^^^^^^^^", CLogger::LEVEL_ERROR);
    	$this->layout = 'index';
		$this->render('index');
	}

	/**
	 * Специальный экшен для авторизации не наших родных пользователей, а внешних пользователей,
	 * которые уже атворизованы на других сервисах/соц-сетях
	 *
	 * @param null $service - сервис через который пытаемся залогинится(vkontakte, twitter,...)
	 * @param null $identity - OpenID который ввёл пользователь(нужно для LiveJournal)
	 */
	public function actionAuthexternals($service=null, $identity=null)
	{
		$redirect = Yii::app()->request->urlReferrer;

		if ($redirect)
		{
			Yii::app()->session['auth_redirect'] = $redirect;
		}


		//Для тех кто случайно сюда попал, редиректим на главную
		if( empty($service) )
		{
			$this->redirect('/');
		}

		$authIdentity = Yii::app()->eauth->getIdentity($service);
		$authIdentity->redirectUrl = Yii::app()->session['auth_redirect'];
		$authIdentity->cancelUrl = $this->createAbsoluteUrl('/login');

		try
		{
			if ($authIdentity->authenticate())
			{
				//Используем специальный класс UserExtIdentity, который реализует всю бизнесс-логику
				//авторизации пользоателя через внешние сервисы такие как vfkontakte, twitter, fb,...
				$identity = new UserExtIdentity($authIdentity);

				$is_new_user = false;

				//Успешная авторизация
				if ($identity->authenticate($is_new_user)) {

					$duration = 3600*24*365; // 365 days = 1 Year

					Yii::app()->user->login($identity, $duration);

					//Новых пользователей редиректим на профайл
					if( $is_new_user && empty(Yii::app()->user->returnUrl))
					{
						$authIdentity->redirectUrl = Yii::app()->session['auth_redirect'];
					}

					// специальное перенаправления для корректного закрытия всплывающего окна
					$authIdentity->redirect();
				}
				else {
					// закрытие всплывающего окна и перенаправление на cancelUrl
					$authIdentity->cancel();
				}
			}
		}
		catch(Exception $e)
		{
			// авторизация не удалась, перенаправляем на страницу входа
			$this->redirect(array('/login'));
		}

		// авторизация не удалась, перенаправляем на страницу входа
		$this->redirect(array('/login'));
	}


	/**
	 * Авторизация наших пользователей
	 */
	public function actionLogin()
	{
		$this->layout = false;
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}

		$model = new User('auth');

		if( isset($_POST['User']) )
			$model->attributes = $_POST['User'];

		if(Yii::app()->request->isAjaxRequest && isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if( isset($_POST['User']) )
		{
			if($model->validate() && $model->login())
			{
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		else
		{
			$_back_url = Yii::app()->request->getUrlReferrer();

			if( !empty($_back_url) && $_back_url != '/login' && strstr($_back_url, '/activate') == false) {
				Yii::app()->user->setReturnUrl( $_back_url );
			}
		}


		if (Yii::app()->request->isAjaxRequest)
			$this->renderPartial('login', array('model'=>$model), false, true);
		else
			$this->render('login', array('model'=>$model));
	}

	/**
	 * Логаут пользователя
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();

		if ( Yii::app()->request->urlReferrer ) {
			$this->redirect(Yii::app()->request->urlReferrer);
		}

		$this->redirect(Yii::app()->homeUrl);
	}

	/*
	 * Регистрация нового пользователя
	 */
	public function actionRegistration()
	{
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}


		$model = new User('register');

		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')=='register-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		$user_form = Yii::app()->request->getParam('User', null);

		if( !empty($user_form) )
		{
			//Так как в интерфейсе убрали поле "Логин", будем заполнять логин=email
			$user_form['username'] = $user_form['email'];

			$model->attributes = $user_form;
			$model->activkey = md5( time() . $user_form['email'] );

			if( $model->save() )
			{
				$content = $this->renderPartial(
								'/email/registration',
								array(
									'user' => $model
								),
								true
							);

				$m = new Mail();
				$m->send($model->email, 'Регистрация на kavpolit.com', $content);

				$this->redirect('/registration/confirm/');
			}
		}

		$this->pageTitle .= ", Регистрация";
		Yii::app()->facebook->ogTags['title'] = $this->pageTitle;

		if (Yii::app()->request->isAjaxRequest)
		{
			$this->renderPartial('register', array('model'=>$model), false, true);
			Yii::app()->end();
		}
		else
		{
			$this->render('register', array('model'=>$model));
		}
	}



	public function actionConfirmRegistration()
	{
		$this->render('confirm_registration');
	}






	public function actionRecovery($code=null, $success=0)
	{
		//Если пользователь авторизован, то ему не доступна страница восстановления пароля
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}


		$this->pageTitle .= ", Восстановление пароля";
		Yii::app()->facebook->ogTags['title'] = $this->pageTitle;

		//В случае успеха, показываем страницу успеха
		if($success)
		{
			$this->render('recovery', array('success'=>$success));
			return;
		}

		//В случае, если пользователь прошёл по ссылке из письма с кодом восстановления пароля, то
		//показываем форму смены пароля
		if( !empty($code) )
		{
			$user = User::model()->find(
				'`recovery_hash`=:code AND (UNIX_TIMESTAMP(NOW())-`recovery_time`<=:hash_ttl)',
				array(':code'=>$code, ':hash_ttl'=>3600*2)
			);

			if( !empty($user) )
			{
				$model = new User('changepwd');

				if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')=='changepwd-form')
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}

				$changepwd_form = Yii::app()->request->getParam('User', null);

				if( !empty($changepwd_form) )
				{
					$model->attributes = $changepwd_form;

					if( $model->validate() )
					{
						$user->recovery_hash = new CDbExpression('NULL');
						$user->recovery_time = new CDbExpression('NULL');

						$salt = UserIdentity::makesalt();

						$user->password = $salt.UserIdentity::encrypting($model->password, $salt);

						if( $user->save() )
						{
							$this->redirect('/recovery/success/');
						}
					}
				}

				$this->render('changepwd', array('model'=>$model, 'code'=>$code));
			}
			else{
				$this->redirect('/recovery/');
			}

			return;
		}

		//Если пользователь прото вошёл на страницу смены пароля, то показываем ему форму восстановления пароля
		$model = new User('recovery');

		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')=='recovery-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		$user_form = Yii::app()->request->getParam('User', null);

		if( !empty($user_form) )
		{
			$model->attributes = $user_form;

			if( $model->validate() )
			{
				$user = User::model()->active()->find(
					'(`email`=:email)',
					array(':email'=>$model->username)
				);

				if( !empty($user) )
				{
					$user->recovery_hash = User::getUniqueValue();
					$user->recovery_time = new CDbExpression('NOW()');

					if( $user->save() )
					{
						$url = Yii::app()->params['baseUrl'] . "/recovery/code/{$user->recovery_hash}/";

						$content = $this->renderPartial(
							'/email/change_password',
							array(
								'url' => $url,
							),
							true
						);


						$m = new Mail();
						$m->send($user->email, 'Смена Вашего пароля на kavpolit.com', $content);


						$this->redirect('/recovery/confirmation/');
					}
					else {
						Yii::log(
							"An error occured during changing the password: " .
							UtilsHelper::inspect($user->getErrors()),
							CLogger::LEVEL_ERROR
						);
					}
				}
				else
				{
					$this->redirect('/recovery/confirmation/');
				}
			}
		}

		$this->render('recovery', array('model'=>$model, 'success'=>$success));
	}




	public function actionReactivate()
	{
		$email = Yii::app()->user->email;
		$model = User::model()->findByAttributes( array('email' => $email, 'status' => User::STATUS_NOACTIVE) );

		if (!empty($model))
		{
			$model->activkey = md5( time() . $email );
			$model->save();

			$content = $this->renderPartial(
							'/email/reactivate',
							array(
								'user' => $model
							),
							true
						);

			$m = new Mail();
			$m->send($model->email, 'Активация аккаунта на kavpolit.com', $content);

			if (Yii::app()->request->isAjaxRequest)
				$this->renderPartial('reactivate');
			else
				$this->render('reactivate');
		}
		else
		{
			throw new CHttpException(404);
		}
	}


	public function actionActivate($code)
	{
		$model = User::model()->findByAttributes( array('activkey' => $code, 'status' => User::STATUS_NOACTIVE) );

		$success = false;

		if (!empty($model))
		{
			$model->status = User::STATUS_ACTIVE;
			$model->activkey = null;
			$success = $model->save();
		}

		$this->render('activate', array('model' => $model, 'success' => $success));
	}


	/**
	 * Страница для показа любых сообщений об ошибках (включая 404).
	 * ТО, что для обработки таких ситуаций будет использоваться именно этот экшен,
	 * прописано в конфиге, в разделе 'errorHandler'=>array('errorAction'=>'site/error').
	 */
	public function actionError()
	{
		$this->layout = 'inner';

		// if ($error = Yii::app()->errorHandler->error)
		{
			if (Yii::app()->request->isAjaxRequest)
			{
				// echo $error['message'];
			}
			else
			{
				$this->render('error');
			}
		}
	}


	public function actionSearch()
	{
		$this->layout = 'inside_2col';
		$this->render('search/yandex');
	}




	/*protected function afterRender($view, &$output)
	{
		parent::afterRender($view, $output);
		//Yii::app()->facebook->addJsCallback($js); // use this if you are registering any $js code you want to run asyc
		Yii::app()->facebook->initJs($output); // this initializes the Facebook JS SDK on all pages
		Yii::app()->facebook->renderOGMetaTags(); // this renders the OG tags
		return true;
	}*/
}
