<?
class ProfileController extends Controller
{
	public $layout = 'inner';


	public function beforeAction($action)
	{
		if (Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->createUrl('site/login'));

		return true;
	}



	/*
	* Метод ответвенный за генерацию и редактирование профайла
	*/
	public function actionIndex()
	{		
		$this->pageTitle = "Личный кабинет";
		Yii::app()->facebook->ogTags['title'] = $this->pageTitle;

		
		//Получить профайл пользователя
		$model = User::model()->with(array('blog'))->findByPk( Yii::app()->user->id );
		$posts = array();
		$pages = null;


		// Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// В случае POST-запроса, производим валидацию и сохранение данных
		if (isset($_POST['User']))
		{
			$model->scenario = 'createblog';
			$model->attributes = $_POST['User'];

			if ($model->save())
			{
				$this->redirect('/my/');
			}
			else
			{
				var_dump($model->getErrors());
			}
		}


		if (!empty($model->blog))
		{
			$model->blog_code = $model->blog->code;
			$model->blog_name = $model->blog->name;

			$criteria = new CDbCriteria();
			$criteria->condition = 't.blog_id = :blog_id AND (t.is_draft = 0 OR (t.is_draft = 1 AND t.draft_for_id = 0))';
			$criteria->params = array(':blog_id' => $model->blog->id);
			$criteria->order = 't.date_create DESC';

			$count = Post::model()->not_draft()->count( $criteria );

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);

			$posts = Post::model()->Indexed()->with(array('draft'))->findAll( $criteria );
		}


		$stat = ViewsFull::model()->getStatistic4Elements($posts, 'post');

		$this->render('index', array('model' => $model, 'posts' => $posts, 'pages' => $pages, 'stat' => $stat));
	}




	public function actionBlog()
	{
		$model = User::model()->with(array('blog'))->findByPk( Yii::app()->user->id );


		// В случае POST-запроса, производим валидацию и сохранение данных
		if (isset($_POST['User']))
		{
			$model->scenario = 'editblog';
			$model->attributes = $_POST['User'];

			// Выдать сообщения об ошибке в случае аякс валидации
			if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'profile-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			if ($model->save())
			{
				$this->redirect('/my/blog/');
			}
			else
			{
				var_dump($model->getErrors());
			}
		}


		$this->render('edit_blog', array('model' => $model));
	}




	public function actionArticles()
	{
		$articles = array();
		$pages = null;

		$criteria = new CDbCriteria();
		$criteria->condition = 'user_id = :user_id';
		$criteria->params = array(':user_id' => Yii::app()->user->id);

		$author = Authors::model()->find($criteria);

		if (empty($author))
			throw new CHttpException(404);

		$criteria = new CDbCriteria();
		$criteria->index = 'id';
		$criteria->order = 'date_create DESC';

		$count = Articles::model()->authorIdIs($author->id)->count($criteria);

		$pages = new CPagination($count);
		$pages->pageSize = 10;
		$pages->applyLimit($criteria);

		$articles = Articles::model()->authorIdIs($author->id)->Indexed()->findAll($criteria);
		$stat = ViewsFull::model()->getStatistic4Elements($articles);

		$this->render('articles', array('articles' => $articles, 'stat' => $stat, 'pages' => $pages));
	}





	public function actionUser($user_id)
	{

		//Получить текущий профайл пользователя
		$model = User::model()->with(array('blog'))->findByPk( $user_id );
		$posts = array();
		$pages = null;


		//Выдать сообщения об ошибке в случае аякс валидации
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'profile-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}


		if (!empty($model->blog))
		{
			$model->blog_code = $model->blog->code;
			$model->blog_name = $model->blog->name;

			$criteria = new CDbCriteria();
			$criteria->condition = 'blog_id = :blog_id';
			$criteria->params = array(':blog_id' => $model->blog->id);
			$criteria->order = 'date_create DESC';

			$count = Post::model()->count( $criteria );

			$pages = new CPagination($count);
			$pages->pageSize = 10;
			$pages->applyLimit($criteria);

			$posts = Post::model()->Indexed()->findAll( $criteria );
		}


		$stat = ViewsFull::model()->getStatistic4Elements($posts, 'post');

		$this->render('user', array('model' => $model, 'posts' => $posts, 'pages' => $pages, 'stat' => $stat));
	}





	public function actionComments()
	{
		$criteria = new CDbCriteria();
		$criteria->select = '*';
		$criteria->condition = 'user_id = :user_id';
		$criteria->params = array(':user_id' => Yii::app()->user->id);
		$criteria->order = 't.date_publish DESC';

		$count = Comments::model()->count( $criteria );

		$pages = new CPagination($count);
		$pages->pageSize = 10;
		$pages->applyLimit($criteria);

		$comments = Comments::model()->with(array('Article', 'Post'))->findAll($criteria);
		$this->render('comments', array('comments' => $comments, 'pages' => $pages, 'count' => $count));
	}



	public function actionSubscriptions()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'watcher_id = :watcher_id';
		$criteria->params = array(':watcher_id' => Yii::app()->user->id);

		$count = Subscription::model()->count($criteria);

		$subscriptions = Subscription::model()->with(array('Author'))->findAll($criteria);

		$this->render('subscriptions', array('subscriptions' => $subscriptions));
	}



	public function actionSubscribers()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'author_id = :author_id';
		$criteria->params = array(':author_id' => Yii::app()->user->id);

		$count = Subscription::model()->count($criteria);

		$subscribers = Subscription::model()->with(array('Watcher'))->findAll($criteria);

		$this->render('subscribers', array('subscribers' => $subscribers));
	}


	public function actionEdit()
	{
		$this->pageTitle = "Личный кабинет";
		Yii::app()->facebook->ogTags['title'] = $this->pageTitle;

		
		//Получить профайл пользователя
		$model = User::model()->with(array('blog'))->findByPk( Yii::app()->user->id );
		$posts = array();
		$pages = null;

		$model->scenario = 'profile';


		//Выдать сообщения об ошибке в случае аякс валидации
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'profile-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		//В случае POST-запроса, производим валидацию и сохранение данных
		if( isset($_POST['User']) )
		{
			//Трюк, чтобы при сохранении не затиралось поле в базе, где хранится аватар.
			$_POST['User']['avatar'] = $model->avatar;

			$model->attributes = $_POST['User'];

			if( $result = $model->validate() )
			{
				$attr_list = $model->getAttributes();

				//Чтобы, если пароль пустой не затереть правильный пароль в БД
				if( isset($_POST['User']) &&  mb_strlen($_POST['User']['password']) == 0)
				{
					unset( $attr_list['password'] );
					unset( $attr_list['password2'] );
				}
				
				if( $model->save(false, array_keys($attr_list)) )
				{
					//В случае успешного обновления данных профайла, - редирект на профайл
					$this->redirect('/my/edit/');
				}
			}
		}


		$this->render('edit', array('model'=>$model));
	}

}