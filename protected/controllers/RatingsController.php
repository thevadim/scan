<?
class RatingsController extends Controller
{

	public function actionRate($entity_name, $entity_id, $rate)
	{

		#if(Yii::app()->request->isAjaxRequest)
		{
			$r = Ratings::model()->rateEntity($entity_name, $entity_id, $rate);
			echo CJSON::encode($r);
		}
		
	}

}