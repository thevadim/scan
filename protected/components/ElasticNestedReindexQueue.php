<?php
class ElasticNestedReindexQueue extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_elastic_nested_reindex';
	}

	public function rules()
	{
		$rules = array(
			array('type, entity_id', 'required'),
			array('entity_id', 'numerical', 'integerOnly' => true),
			array('type', 'length', 'max' => 100),
		);

		return $rules;
	}


	public static function addTask($path, $match_rule)
	{
		try
		{
			$search = new \YiiElasticSearch\Search(Yii::app()->name, 'articles,posts');
			$search->filter = array(
				'nested' => array(
					'path' => $path,
					'query' => array(
						'bool' => $match_rule
					)
				)
			);

			$result_set = Yii::app()->elasticSearch->search($search);
			$search_results = $result_set->getResults();

			$pack = array();
			foreach ($search_results as $p)
			{
				$pack[] = '(' . implode(', ', array('\'' . $p->getType() . '\'', $p->getId())) . ')';
			}

			if (!empty($pack))
			{
				$packs =  implode(', ', $pack);

				$sql = '
						INSERT INTO bg_elastic_nested_reindex
						(type, entity_id)
						VALUES
						' . $packs . '
						ON DUPLICATE KEY UPDATE entity_id = entity_id';

				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}
		}
		catch(Exception $e)
		{
			Yii::log('ElasticNestedReindexQueue:addTask ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}


	public function apply()
	{
		try 
		{
			if ($this->type == 'posts')
			{
				$e = Post::model()->findByPk($this->entity_id);
				if (!empty($e))
				{
					$e->name = $e->name;
					$e->save();
				}
			}

			if ($this->type == 'articles')
			{
				$e = Articles::model()->findByPk($this->entity_id);
				if (!empty($e))
				{
					$e->name = $e->name;
					$e->save();
				}
			}

			$this->delete();
		}
		catch (Exception $e)
		{
			Yii::log('ElasticNestedReindexQueue:apply ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}
}