<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 18.03.13
 * Time: 14:18
 *
 * Дописываем к нашей собственной статистике о времени выполнения контроллеров_экшенов и виджетов
 * ещё и SQL запросы, которые были на странице.
 */

class BgProfileLogRoute extends CProfileLogRoute {

    public $logTableName;

    public function processLogs($logs)
    {
        /**
         * Обрабатываем логи, только если включен режим профилирования!
         */
        if( !Controller::doProfile() )
        {
            return;
        }

        try
        {
            ob_start();

            parent::processLogs($logs);

            $result = ob_get_contents();

            ob_end_clean();

            $request_code = Controller::getRequestCode();

            $sql = "UPDATE {$this->logTableName} SET sql_stat = :result WHERE request_code = '{$request_code}' AND category = 'bg.profiling.controllers';";

            Yii::app()->db->createCommand($sql)->execute( array(':result' => $result) );
        }
        catch(Exception $e)
        {
            Yii::log(
                "Exception in 'SlonProfileLogRoute': {$e->getMessage()}, {$e->getFile()}, {$e->getLine()}",
                CLogger::LEVEL_ERROR
            );
        }
    }
}