<?php

/**
 * Наш собственный класс юзера
 */
class MyWebUser extends RWebUser /*ВАЖНО! Наследуемся от модуля Rights*/
{

	public function afterLogin($fromCookie)
	{
		parent::afterLogin($fromCookie);

		/*Обновляем дату последнего посещения*/
		$user = User::model()->active()->findByPk($this->getId());
		if ($user)
		{
			$user->lastvisit = new CDbExpression('NOW()');
			$user->save();

			if (!empty(Yii::app()->user))
			{
				//Обновляем данные в сессии, так как они могли измениться
				foreach ($user->attributes as $name => $value) {
					if (in_array($name, array('id', 'activkey', 'password'))) {
						continue;
					}

					Yii::app()->user->setState($name, is_numeric($value) ? $value + 0 : $value);
				}

			}
		}
		//Если авторизовываемся по куке то восстанавливаем данные пользователя
		if ($fromCookie)
		{
			$identity = new UserIdentity("", "");

			$identity->saveUserDataInSession($user);
		}
	}

}