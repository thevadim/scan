<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 22.03.13
 * Time: 18:06
 *
 * Использую эту каптчу в форме подписки.
 * Перекрыть Run пришлось, чтобы каптча обновлялась
 * каждый get-запрос!
 *
 */

class CCaptchaActionV2 extends CCaptchaAction {

    public function run()
    {
        $code = $this->getVerifyCode(true);

        if(isset($_GET[self::REFRESH_GET_VAR]))  // AJAX request for regenerating code
        {
            echo CJSON::encode(array(
                'hash1'=>$this->generateValidationHash($code),
                'hash2'=>$this->generateValidationHash(strtolower($code)),
                // we add a random 'v' parameter so that FireFox can refresh the image
                // when src attribute of image tag is changed
                'url'=>$this->getController()->createUrl($this->getId(),array('v' => uniqid())),
            ));

            Yii::app()->end();
        }

        $this->renderImage($this->getVerifyCode());

        Yii::app()->end();
    }
}