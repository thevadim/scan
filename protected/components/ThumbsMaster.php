<?
// Получалка изображения любого размера
// Если изображения нет - создаем задачу на пережатие (ThumbsMasterTask)

class ThumbsMaster
{
	const SAVE_PATH = '/media/thumbs';
	const SAVE_REMOTE_PATH = '/media/thumbs/remote';
	const DEFAULT_IMAGE = '/static/css/pub/i/logo_dummy.png';

	// Настройки превьюшек
	static $settings = array(
							'150_auto'  => array('jpeg_quality' => 90, 'image_resize' => true, 'image_x' => 150, 'image_ratio_y' => true, 'file_overwrite' => true),
							'200_auto'  => array('jpeg_quality' => 90, 'image_resize' => true, 'image_x' => 200, 'image_ratio_y' => true, 'file_overwrite' => true),	
							'x_auto_150_crop'  => array('jpeg_quality' => 90, 'image_resize' => true, 'image_y' => 150, 'file_overwrite' => true, 'image_ratio_crop' => true,),	

							);



	// $image - ссылка на изображение в виде http://domain.com/image.jpg или /media/upload/image.jpg
	// $settings - указываем $this->settings['key']
	// $immediate - если true, то пережимаем сразу же, без постановки задачи на крон
	public static function getThumb($image, $settings, $immediate = false, $noimage_path = null)
	{
		$key = self::getFolderName($settings);

		$image = str_replace(Yii::app()->request->hostInfo, '', $image);

		if (mb_strlen($image) == 0)
		{
			$image = !empty($noimage_path) ? $noimage_path : self::DEFAULT_IMAGE;
		}

		//if (mb_strpos($image, 'http://') === false)
		if (preg_match('|https?://|', $image) != 1)
		{
			if (!file_exists( Yii::getPathOfAlias('webroot') . $image))
			{
				$image = !empty($noimage_path) ? $noimage_path : self::DEFAULT_IMAGE;
			}
		}

		if (!$key)
			return $image;

		$ext_explode = explode('.', $image);
		$ext = end($ext_explode);
		$hash = md5($image);
		$file_name = $hash . '.' . $ext;

		$folder1 = substr($hash, 0, 2);
		$folder2 = substr($hash, 2, 2);
		$folder3 = substr($hash, 4, 2);

		$expected_path = self::SAVE_PATH . '/' . $key . '/' . $folder1 . '/' . $folder2 . '/' . $folder3;
		$expected_file_path = $expected_path . '/' . $file_name;


		// Возвращаем обжатый файл, если есть
		if (file_exists( Yii::getPathOfAlias('webroot') . $expected_file_path))
			return $expected_file_path;

		$lock = Yii::app()->cache->get('ThumbsMaster-' . $expected_path.$hash);
		// Возвращаем непережатый оригинал, если превью еще не готова
		if ($lock !== false)
			return $image;

		// Если иконка не существует
		if (!file_exists( Yii::getPathOfAlias('webroot') . $expected_file_path))
		{
			// блокировка - отдаем необжатый оригинал, пока обжимается превью
			if(!$immediate)
			{
				Yii::app()->cache->set('ThumbsMaster-' . $expected_path.$hash, true, 3600);
			}

			$task = new ThumbsMasterTask();
			$task->hash = $hash;
			$task->ext = $ext;
			$task->original_path = $image;
			$task->compressed_path = $expected_path;
			$task->settings = json_encode($settings);

			if ($immediate)
			{
				$result = $task->apply();
				if ($result !== false && file_exists( Yii::getPathOfAlias('webroot') . $result))
				{
					$image = $result;
				}
			}
			else
				$task->save();
		}


		// Если изображение еще не пережато - возвращаем оригинал
		return $image;
	}



	private static function getFolderName($settings)
	{
		foreach (self::$settings as $key => $value)
		{
			if ($value === $settings)
				return $key;
		}

		return 'custom/' . substr(md5(implode('', $settings)), 0, 4);
	}
}







