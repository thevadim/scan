<?php
/**
 * User: Dmitry Rusakov
 * Date: 21.05.13
 * Time: 17:07
 */

class MaterialRequest extends CActiveRecord {

    /**
     * @param string $className
     *
     * @return Invoice
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'material_request';
    }

    public function scopes()
    {
        return array(

        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = array(
            array('username, userphone, usercompany, usermail', 'safe'),
        );

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            //
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
        );

        return $atr;
    }
}