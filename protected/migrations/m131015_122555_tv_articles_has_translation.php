<?php

class m131015_122555_tv_articles_has_translation extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bg_articles_draft', 'has_translation', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER has_video');
		$this->addColumn('bg_articles', 'has_translation', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER has_video');
		//ALTER TABLE  `bg_articles_draft` ADD  `has_translation` TINYINT( 1 ) NOT NULL AFTER  `has_video` ;
		
	}

	public function down()
	{
		$this->dropColumn('bg_articles_draft', 'has_translation');
		$this->dropColumn('bg_articles', 'has_translation');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}