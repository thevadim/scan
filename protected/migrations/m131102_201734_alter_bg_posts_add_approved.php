<?php

class m131102_201734_alter_bg_posts_add_approved extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_posts', 'approved', 'TINYINT(1) NOT NULL DEFAULT 0');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_posts', 'approved');
	}
}