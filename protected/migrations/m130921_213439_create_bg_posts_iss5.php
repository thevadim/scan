<?php

class m130921_213439_create_bg_posts_iss5 extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_posts',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'active'            => 'TINYINT(1) DEFAULT 1',
				 'blog_id'           => 'INTEGER NOT NULL',
				 'name'              => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'preview_text'      => 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'detail_text'       => 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'cover'     		 => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'date_create'		 => 'DATETIME',
			)
		);

		$this->createIndex('blog_id', 'bg_posts', 'blog_id');
		$this->createIndex('active', 'bg_posts', 'active');
		$this->createIndex('date_create', 'bg_posts', 'date_create');
	}

	public function safeDown()
	{
		$this->dropTable('bg_posts');
	}
}