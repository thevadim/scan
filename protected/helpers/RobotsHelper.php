<?php

class RobotsHelper{

    //Массив поисковиков(содержит часть user_agent'a)
    private static $se_user_agents = array(
        'Yandex',
        'YandexNews',
        'YandexBot',
        'Googlebot',
        'Twitter',
        'msnbot',
        'YaDirectBot',
        'Twiceler',
        'Yanga',
        'Yahoo',
        'RssFarm.com',
        'MJ12bot',
        'StackRambler',
        'Python-urllib',
        'MJ12bot',
        'Aport',
        'Slurp',
        'MSNBot',
        'Teoma',
        'Scooter',
        'ia_archiver',
        'Lycos',
        'Mail',
        'WebAlta',
        'Accoona',
        'antabot',
        'Baiduspider',
        'EltaIndexer',
        'GameSpyHTTP',
        'Gigabot',
        'gsa-crawler',
        'Gulper Web Bot',
        'MihalismBot',
        'OmniExplorer_Bot',
        'Pagebull',
        'Jigsaw',
        'Wget',
        'YahooFeedSeeker',
        'BLP_bbot',
        'Amppari',
        'JS-Kit',
        'PycURL',
        'PostRank',
        'Butterfly',
        'Birubot',
        'suggybot',
    );

    /*
     * Проверяем есть ли в заголовке запроса признаки бота и если да, возвращаем истину, - то есть
     * обратившейся за страницей клиент, - бот.
     */
    public static function isBot()
    {
        if( !isset($_SERVER['HTTP_USER_AGENT']) )
        {
            return false;
        }

        foreach(self::$se_user_agents as $uagent)
        {
            if( strpos($_SERVER['HTTP_USER_AGENT'], $uagent) !== false )
            {
                return true;
            }
        }

        return false;
    }
}