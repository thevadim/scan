<?php

class ProfileHelper{
    /*
     * Достаём Фамилию-Имя пользователя по его id
     */
    public static function getFIO($user_id)
    {
        if( empty($user_id) )
        {
            return "";
        }

        $profile = User::model()->findByPk($user_id);

        if( empty($profile) )
        {
            return "";
        }

        $fio = trim($profile->lastname . ' ' . $profile->firstname);

        return $fio;
    }
}