<?php

require_once dirname(dirname(__FILE__)).'/EOpenIDService.php';

class LivejournalOpenIDService extends EOpenIDService
{
    protected $name = 'livejournal';
    protected $title = 'Livejournal';
    protected $type = 'OpenID';
    protected $jsArguments = array(/*'popup' => array('width' => 880, 'height' => 520)*/); //Не используем, задаётся вручную в auth.php

    protected $url = 'www.livejournal.com'; //= 'http://kopita-slona.livejournal.com/';
    protected $requiredAttributes = array(
        /*ЖЖ никаких данных не отдаёт, поэтому всё комментрим*/
        //'name' => array('firstname', 'namePerson/first'),
        //'lastname' => array('lastname', 'namePerson/last'),
        //'email' => array('email', 'contact/email'),
        //'language' => array('language', 'pref/language'),
    );

    protected function fetchAttributes()
    {
        /*ЖЖ никаких данных не отдаёт, поэтому всё комментрим*/
         //$this->attributes['fullname'] = $this->attributes['name'].' '.$this->attributes['lastname'];
    }

    public function setUrl($the_url)
    {
        $this->url = $the_url;
    }
}
