(function($){
    jQuery.fn.topLink = function (settings) {
        settings = jQuery.extend({
            min:50,
            fadeSpeed:500,
            ieOffset:50,
            scrollSpeed:300,
            easingType:'linear'
        }, settings);
        return this.each(function () {
            //listen for scroll
            var el = $(this);
            el.hide().click(function(){
                $('html, body').animate({scrollTop:0}, settings.scrollSpeed, settings.easingType);
                return false;
            });
            $(window).scroll(function () {
                //stupid IE hack
                if (!jQuery.support.hrefNormalized) {
                    el.css({
                        'position':'absolute',
                        'top':$(window).scrollTop() + $(window).height() - settings.ieOffset
                    });
                }
                if ($(window).scrollTop() >= 0.5*$(window).height()) {
                    el.fadeIn(settings.fadeSpeed);
                }
                else {
                    el.fadeOut(settings.fadeSpeed);
                }
            });
        });
    };

})(jQuery);