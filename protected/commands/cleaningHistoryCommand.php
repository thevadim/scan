<?php
/**
 * Чистим историю сохранения статей
 * Запускать так: /usr/local/bin/php /home/bg.ru/www/protected/console.php cleaninghistory Lastdays --days=30
 */
class cleaningHistoryCommand extends CConsoleCommand
{
	public function actionLastDays($days)
	{
		$time     = microtime(true);
		$limit    = 10000;
		$articles = Yii::app()->db->createCommand('SELECT id FROM bg_articles_draft WHERE is_dummy=0 AND x_timestamp < NOW() - INTERVAL ' . intval($days) . ' DAY LIMIT ' . $limit)->queryColumn();
		foreach ($articles as $article_draft_id)
		{
			Yii::app()->db->createCommand('DELETE FROM bg_elements_draft WHERE article_draft_id=' . $article_draft_id)->execute();
			Yii::app()->db->createCommand('DELETE FROM bg_chapters_draft WHERE article_draft_id=' . $article_draft_id)->execute();
			Yii::app()->db->createCommand('DELETE FROM bg_articles_draft WHERE id=' . $article_draft_id)->execute();
		}
		//echo count($articles);
		echo microtime(true) - $time;
		echo "\n\n";
	}
}