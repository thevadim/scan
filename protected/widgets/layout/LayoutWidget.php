<?
/*
 * Содержит куски оформления сайта, меню, шапку, футер
 */
class LayoutWidget extends CWidget
{
	public $template;

    public function init()
    {

    }
 
    public function run()
    {
		if(!is_null($this->template))
		{
			try
			{
				$this->render($this->template);
			}
			catch (Exception $e)
			{
				
			}
		}
    }
}
?>