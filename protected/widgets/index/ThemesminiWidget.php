<?
class ThemesminiWidget extends CWidget
{

	public function init()
	{

	}

	public function run()
	{
		$CACHE_KEY = 'ThemesminiWidget';
		$items = Yii::app()->cache->get($CACHE_KEY);

		if($items === false)
		{
			$items = IndexpageSeries::model()->with(array('series'))->order('order_num ASC')->findAll();

			Yii::app()->cache->set($CACHE_KEY, $items, 60);
		}

		$this->render('themesmini', array('items' => $items));
	}

}