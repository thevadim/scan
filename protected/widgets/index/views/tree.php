<?
$n = 0;
foreach ($tree as $tree_key => $t)
{
	if ($n == 0)
	{
		?>
		<div class="columns">
		<?
	}
	?>


	<div class="col _w32">
		<header class="section-title _marked _mtb30p">
			<h2><a href="<?= $t->tree->url() ?>"><?= $t->tree->name ?></a></h2>
		</header>

		<div class="anons-list _simple">
		<?
		foreach ($items_holder[$tree_key] as $item)
		{
			?>
			<div class="anons">
				<?
				if (strlen($item->preview_img) > 0)
				{
					?>
					<a href="<?= $item->url() ?>" class="piclink">
						<img class="pic" src="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['110_64'])?>" alt="" width="110" height="64">
					</a>
					<?
				}
				?>
				<a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a>
			</div>
			<?
		}
		?>
		</div>
	</div>


	<?
	++$n;

	if ($n == 3)
	{
		$n = 0;
		?>
		</div><!-- /.columns -->

		<?
		if ($tree_key + 1 != count($tree))
		{
			?>
			<div class="columns _blackborders">
				<div class="col _w32"></div>
				<div class="col _w32"></div>
				<div class="col _w32"></div>
			</div><!-- /.columns._borders -->
			<?
		}
	}

	
}



