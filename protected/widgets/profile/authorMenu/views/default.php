<?
$aid = yii::app()->controller->action->id;
?>
<div class="b-filter">
	<ul class="b-filterlist _tabs">
		<li class="tablink <?= $aid == 'detail' ? 'active' : '' ?>"><a href="<?= $author->url() ?>">Статьи <span>(<?= $articles ?>)</span></a></li>
		<?
		if (!empty($author->User) && !empty($author->User->blog))
		{
			?><li class="tablink <?= $aid == 'posts' ? 'active' : '' ?>"><a href="<?= $author->url() ?>posts/">Посты <span>(<?= $posts ?>)</span></a></li><?	
		}
		
		if (!empty($comments))
		{
			?><li class="tablink <?= $aid == 'comments' ? 'active' : '' ?>"><a href="<?= $author->url() ?>comments/">комментарии <span>(<?= $comments ?>)</span></a></li><?
		}

		if (!empty($author->User) && !empty($author->User->blog))
		{
			?><li class="tablink <?= $aid == 'subscriptions' ? 'active' : '' ?>"><a href="<?= $author->url() ?>subscriptions/">подписки <span>(<?= $subscriptions ?>)</span></a></li>
			<li class="tablink <?= $aid == 'subscribers' ? 'active' : '' ?>"><a href="<?= $author->url() ?>subscribers/">подписчики <span>(<span id="subscriptions_cnt"><?= $subscribers ?></span>)</span></a></li>
			<?
		}
		?>
	</ul>
</div>