<div class="profile">
	<?
	// if (!empty($user->avatar))
	{
		?>
		<div class="photo">
			<img src="<?= ThumbsMaster::getThumb($user->avatar, ThumbsMaster::$settings['260_260'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="" />
		</div>
		<?
	}
	?>

	<div class="info_wrp">
		<div class="info">
			<p class="name">
				<?= $user->firstname ?> <?= $user->lastname ?>
				<a href="/my/edit/" class="profile_edite _blue">Редактировать профиль</a><br>
				<?
				if (!empty($user->blog))
				{
					?><a href="/my/blog/" class="blog_edite _blue">Настройки блога</a><?
				}
				?>
			</p>
			<p>
				<?
				if (!empty($user->blog))
				{
					?><span class="login"><?= $user->blog->code ?></span><?
				}
				?>
				<span class="_grey">на Кавполит с <?= Yii::app()->dateFormatter->format("dd MMMM yyyy", $user->createtime) ?> г.</span>
			</p>
			
			<p><span class="num"><?= $user->rating ?></span>  <span class="_grey">Ваш рейтинг</span></p>
			
			<?
			if (!empty($user->blog))
			{
				?>
				<p>
					<span class="_grey">Адрес блога: <a href="<?= $user->blog->url() ?>" class="_blue"><?= $user->blog->url() ?></a></span>
				</p>
				<?
			}
			?>
		</div>

		<?
		if (!empty($user->birthday))
		{
			$birthday = Yii::app()->dateFormatter->format("dd MMMM yyyy", $user->birthday);
			?>
			<div class="col"><span class="_grey">Дата рождения:</span><?= $birthday ?></div>
			<?
		}
		?>
		

		<?
		if (!empty ($user->country) || !empty($user->city))
		{
			?>
			<div class="col"><span class="_grey">Место проживания:</span><?= $user->country ?><?= !empty($user->country) && !empty($user->city) ? ',' : '' ?> <?= $user->city ?></div>
			<?
		}
		?>

		<div class="col"><span class="_grey">E-mail:</span><a href="#" class="_blue"><?= $user->email ?></a></div>
	</div>
</div>