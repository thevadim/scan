<div class="profile">
	<?
	if (!empty($user) && !empty($user->avatar))
	{
		?>
		<div class="photo">
			<img src="<?= ThumbsMaster::getThumb($user->avatar, ThumbsMaster::$settings['260_260']); ?>" alt="" />
		</div>
		<?
	}
	else if (!empty($author->image))
	{
		?>
		<div class="photo">
			<img src="<?= ThumbsMaster::getThumb($author->image, ThumbsMaster::$settings['260_260'], true); ?>" alt="" />
		</div>
		<?
	}
	?>

	<div class="info_wrp">
		<div class="info1">
		<?
		if (!Yii::app()->user->isGuest && !empty($user->id) && Yii::app()->user->id != $user->id)
		{
			?>
			<p class="name">
				<a href="/blogs/authors/<?= $user->id ?>/unsubscribe/" class="profile_follow _red <?= $watcher_is_subscribed == false ? 'hidden' : '' ?>" onclick="subscription.toggle($(this)); return false;">Отписаться</a>

				<a href="/blogs/authors/<?= $user->id ?>/subscribe/" class="profile_follow _blue <?= $watcher_is_subscribed == true ? 'hidden' : '' ?>" onclick="subscription.toggle($(this)); return false;">Подписаться</a>
			</p>
			<?
		}


		if (!empty($user))
		{
			?>
			<p><span class="num"><?= $user->rating ?></span>  <span class="_grey">Рейтинг</span></p>
			<?

			if (!empty($birthday))
			{
				$birthday = Yii::app()->dateFormatter->format("dd MMMM yyyy", $user->birthday);
				?>
				<div class="col"><span class="_grey">Дата рождения:</span><?= $birthday ?></div>
				<?
			}
			

			if (!empty ($user->country) || !empty($user->city))
			{
				?>
				<div class="col"><span class="_grey">Место проживания:</span><?= $user->country ?><?= !empty($user->country) && !empty($user->city) ? ',' : '' ?> <?= $user->city ?></div>
				<?
			}
		}
		?>
		</div>
	</div>
</div>