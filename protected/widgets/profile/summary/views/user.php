<div class="profile">
	<?
	// if (!empty($user->avatar))
	{
		?>
		<div class="photo">
			<img src="<?= ThumbsMaster::getThumb($user->avatar, ThumbsMaster::$settings['260_260'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="" />
		</div>
		<?
	}
	?>

	<div class="info_wrp">
		<div class="info">
			<p class="name">
				<?= $user->firstname ?> <?= $user->lastname ?>
				<?
				if (!Yii::app()->user->isGuest && Yii::app()->user->id != $user->id)
				{
					?>
					<a href="/blogs/authors/<?= $user->id ?>/unsubscribe/" class="profile_follow _red <?= $watcher_is_subscribed == false ? 'hidden' : '' ?>" onclick="subscription.toggle($(this)); return false;">Отписаться</a>

					<a href="/blogs/authors/<?= $user->id ?>/subscribe/" class="profile_follow _blue <?= $watcher_is_subscribed == true ? 'hidden' : '' ?>" onclick="subscription.toggle($(this)); return false;">Подписаться</a>
					<?
				}
				?>
			</p>
			<p>
				<?
				if (!empty($user->blog_code))
				{
					?><span class="login"><?= $user->blog_code ?></span><?
				}
				?>
				<span class="_grey">на Кавполит с <?= Yii::app()->dateFormatter->format("dd MMMM yyyy", $user->createtime) ?> г.</span>
			</p>
			
			<p><span class="num"><?= $user->rating ?></span>  <span class="_grey">Рейтинг</span></p>
			
			<?
			if (!empty($user->blog))
			{
				?>
				<p>
					<span class="_grey">Адрес блога: <a href="<?= $user->blog->url() ?>" class="_blue"><?= $user->blog->url() ?></a></span>
				</p>
				<?
			}
			?>
		</div>

		<?
		if (!empty($birthday))
		{
			$birthday = Yii::app()->dateFormatter->format("dd MMMM yyyy", $user->birthday);
			?>
			<div class="col"><span class="_grey">Дата рождения:</span><?= $birthday ?></div>
			<?
		}
		?>
		

		<?
		if (!empty ($user->country) || !empty($user->city))
		{
			?>
			<div class="col"><span class="_grey">Место проживания:</span><?= $user->country ?><?= !empty($user->country) && !empty($user->city) ? ',' : '' ?> <?= $user->city ?></div>
			<?
		}
		?>
	</div>
</div>