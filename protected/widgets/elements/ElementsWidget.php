<?
/**
 * Виджет для работы с элементами в админской части
 */
class ElementsWidget extends CWidget
{

	//Режим редактирования или отображения (pub, pub_db и admin)
	//pub отображает элемент используя только переданные параметры
	//pub_db отображает элемент, выбирая его параметры из базы по element_id
	//admin отображает форму редактирования элемента выбирая его параметры из базы
	public $mode = 'pub';

	//ID статьи
	public $article_id;

	//ID элемента
	public $element_id;

	//Тип элемента
	public $type;

	//Содержание элемента
	public $content;

	//Порядковый номер главы
	public $chapterOrderNum;

	//Порядковый номер элемента
	public $elementOrderNum;

	//Параметры, которые будут дополнительно переданы в шаблон элемента
	protected $render_params = array();

	public function init()
	{
		die('elements widget');
	}

	public function run()
	{
		
		$params['elementType'] = $this->type;
		$params['elementId'] = $this->element_id;
		$params['content'] = $this->content;
		$params['chapterOrderNum'] = $this->chapterOrderNum;
		$params['elementOrderNum'] = $this->elementOrderNum;
		$e = ElementBuilder::build($params);		
		$e->render();
		/*
		$element = new Element($this->type);
		$element->setElementId($this->element_id);
		$element->setContent($this->content);
		$element->setRenderMode('pub');		
		$element->render();
		*/
		
		#echo $this->type . '<br>';
		#if (isset(CElements::$types[$this->type]))
		/*{
			//Берем контент элемента из базы
			if ($this->mode !== 'pub' && intval($this->element_id) > 0)
			{
				$element       = ElementsDraft::model()->findByPk($this->element_id);
				$this->content = $element->content;
			}


			$this->render_params['element_id'] = $this->element_id;
			$this->render_params['content']    = $this->content;
			$this->render_params['type']       = $this->type;


			//Отображаем виджет
			$this->render('parts/widgetBegin', $this->render_params);

			if ($this->mode == 'admin')
			{
				//Дополнительные параметры для элемента(выбор голосовалки или галереи и прочее)
				$a                               = CElements::instance($this->type)->getRenderParams($this->content);
				$this->render_params['params']   = $a['params'];
				$this->render_params['a_params'] = $a['a_params'];
				$this->render_params['suffix']   = '_element_id_' . $this->element_id;
				$this->render('admin/' . $this->type, $this->render_params);
			}
			else
			{
				echo CElements::instance($this->type)->parseToAdmin(CJSON::decode($this->content));
			}

			$this->render('parts/widgetEnd', $this->render_params);
		}*/
	}
}