<?php
class ElementFields {
	
	private $_data;
	
	public function __construct(Element $element)
	{
		$fields = ElementFields::decode($element->getContent());
		if(is_array($fields))
		{
			foreach($fields as $field => $value)
			{
				$this->_data[$field] = $value;
			}
		}
	}
	
	public function __get($name)
	{
		if(isset($this->_data[$name]))
			return $this->_data[$name];
		else
			return null;			
	}
	
	public function __set($name, $value)
	{
		$this->_data[$name] = $value;
	}
	
	public function __isset($name)
	{
		return isset($this->_data[$name]);
	}
	
	
	static function decode($content)
	{
		return CJSON::decode($content);
	}
	
	static function encode($data)
	{
		return FnlHelper::utf_json_encode($data);//json_encode($data, JSON_UNESCAPED_UNICODE);
	}
}