<blockquote class="b-article-element b-article-element-quote">
	<p class="b-article-element-quote__content g-color-text">
		<div id="<?=$E->fieldFormId('text')?>" data-name="text" contenteditable="true" data-wysiwyg_config="blockquote.js" data-wysiwyg_inline="true" class="wysiwyg_editor b-article-element-quote__content g-color-text">
			<?=$E->fields->text?>
		</div>
	</p>
</blockquote>

<?

/*

<div>
	<?
	$text = isset($params->text) ? ($params->text) : '';
	echo CHtml::textArea("text" . $suffix,
		$text,
		array('class'             => 'blockquote_text',
				'data-name'         => 'text',
				'style'             => 'width:85%;height:70px;',
				'data-limit-length' => 'blockquote' . $suffix
		)
	);
	$count = mb_strlen($text);
	$class = ($count <= 160) ? 'alert-success' : 'alert-error';
	?>
	<span data-limit-length="blockquote<?=$suffix?>" class="alert <?=$class?> add-on" style="padding:1px 5px"><?=$count?></span>
</div>
*/
?>