<div class="b-article-element b-article-element_shiftable b-article-element-good">
<?php if (mb_strlen($E->fields->video) > 0) { ?>
<div class="b-article-element-good__video">
	<?=UtilsHelper::handlerIframe($E->fields->video)?>
</div>
<?php } ?>
<? if ($E->fields->image_position == 'left') : ?>
	<table class="b-article-element-good__layout">
		<tbody>
			<tr>
				<td class="b-article-element-good__info">
					<?if(mb_strlen($E->fields->image_path) > 0){?>
					<img src="<?=$E->fields->image_path?>" alt="<?=htmlspecialchars($E->fields->name)?>" class="b-article-element-good__image" width="180" />
					<?}?>
					<p class="b-article-element-good__datetime"><?=$E->fields->datetime?></p>
					<p class="b-article-element-good__location-name b-article-element-good__location-name_afisha"><?=$E->fields->facility_name?></p>
					<?if(mb_strlen($E->fields->metro) > 0){?><p class="b-article-element-good__location-subway"><i class="g-icon g-icon_subway"></i>&nbsp;<?=$E->fields->metro?></p><?}?>
					<p class="b-article-element-good__location-address g-font-small-5"><?=$E->fields->address?></p>
					<p class="b-article-element-good__location-additional g-font-small-5"><?=$E->fields->additional_info?></p>
					<?if(mb_strlen($E->fields->site_text) > 1 ){?><p class="b-article-element-place__site"><noindex><a target="_blank" rel="nofollow" href="<?=($E->fields->site_link)?>"><?=($E->fields->site_text)?></a></noindex></p><?}?>
					<? if (mb_strlen($E->fields->price) > 0) : ?><span class="b-article-element-good__price b-article-element-good__price_afisha g-color-text g-font-title-2"><?=$E->fields->price?></span><? endif; ?>
				</td>
				<td class="b-article-element-good__gap"></td>
				<td class="b-article-element-good__main">
					<div class="b-article-element-good__head">
						<p class="b-article-element-good__title g-font-title-2"><?=$E->fields->name?></p>
					</div>
					<p class="b-article-element-good__description"><?=$E->fields->description?></p>
				</td>
			</tr>
		</tbody>
	</table>
<? else : ?>
	<?if(mb_strlen($E->fields->image_path) > 0){?>
	<img src="<?=$E->fields->image_path?>" alt="<?=htmlspecialchars($E->fields->name)?>" class="b-article-element-good__image" width="100%" />
	<?}?>
	<table class="b-article-element-good__layout">
		<tbody>
			<tr>
				<td class="b-article-element-good__info">
					<p class="b-article-element-good__datetime"><?=$E->fields->datetime?></p>
					<p class="b-article-element-good__location-name b-article-element-good__location-name_afisha"><?=$E->fields->facility_name?></p>					
					<?if(mb_strlen($E->fields->metro) > 0){?><p class="b-article-element-good__location-subway g-color-text"><i class="g-icon g-icon_subway_red"></i>&nbsp;<?=$E->fields->metro?></p><?}?>
					<p class="b-article-element-good__location-address g-font-small-5"><?=$E->fields->address?></p>
					<p class="b-article-element-good__location-additional g-font-small-5"><?=$E->fields->additional_info?></p>
					<?if(mb_strlen($E->fields->site_text) > 1 ){?><p class="b-article-element-place__site"><noindex><a target="_blank" rel="nofollow" href="<?=($E->fields->site_link)?>"><?=($E->fields->site_text)?></a></noindex></p><?}?>
					<? if (mb_strlen($E->fields->price) > 0) : ?><span class="b-article-element-good__price b-article-element-good__price_afisha g-color-text g-font-title-2"><?=$E->fields->price?></span><? endif; ?>
				</td>
				<td class="b-article-element-good__gap"></td>
				<td class="b-article-element-good__main">
					<div class="b-article-element-good__head">
						<p class="b-article-element-good__title g-font-title-2"><?=$E->fields->name?></p>
					</div>
					<p class="b-article-element-good__description"><?=$E->fields->description?></p>
				</td>
			</tr>
		</tbody>
	</table>
<? endif; ?>
</div>