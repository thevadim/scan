<?//для отображения точки на карте:?>
<?#=($c->key)?>
<?#=($c->chapter_order_num)?>
<?

if(isset($E->fields->key) || 1==1)
{
?>

<? if (strlen($E->fields->image_path) > 0 && $E->fields->size == 'big') { ?>
<div class="b-article-element b-article-element_shiftable">
	<img src="<?=($E->fields->image_path)?>" class="b-article-element-place__large-image" />
</div>
<? }
$elem_chapter = 'data-chapter="' . $E->getChapterId() . '"';
?>
<div class="b-article-element b-article-element_shiftable b-article-element-place g-color-guide g-color-border">
	<table class="b-article-element-place__layout">
		<tbody>
			<tr>
				<td class="b-article-element-place__location-holder border g-color-border">
					<?if(strlen($E->fields->image_path) > 0 && $E->fields->size == 'small' ){?><img src="<?=($E->fields->image_path)?>" class="b-article-element-place__small-image" /><?}?>
					<div class="b-article-element-place__location g-color-border">
						<i class="g-icon g-icon_info"></i>
						<i class="b-article-element-place__location__line g-color-border"></i>
						<?if(strlen($E->fields->metro) > 0){?><p class="b-article-element-place__location-subway"><i class="g-icon g-icon_subway"></i>&nbsp;<?=($E->fields->metro)?></p><?}?>
						<p class="b-article-element-place__location-address g-font-small-5"><?=($E->fields->address)?></p>
						<p class="b-article-element-place__location-additional g-font-small-5"><?=nl2br($E->fields->additional_info)?></p>
						<?
							if (isset($E->fields->site_text) && is_array($E->fields->site_text))
							{
								foreach ($E->fields->site_text as $key => $site_text)
								{?>
									<p class="b-article-element-place__site">
										<noindex>
											<a target="_blank" rel="nofollow" href="<?=($E->fields->site_link[$key])?>"><?=($site_text)?></a>
										</noindex>
									</p>
								<?}
								?>

							<?}
							elseif ($E->fields->site_text)
							{?>
								<p class="b-article-element-place__site">
									<noindex>
										<a target="_blank" rel="nofollow" href="<?=($E->fields->site_link)?>"><?=($E->fields->site_text)?></a>
									</noindex>
								</p>
							<?}
						?>
						<i class="b-article-element-place__location__bottom"></i>
					</div>
				</td>
				<td rowspan="2" class="b-article-element-place__info-handler border g-color-border">
					<p class="b-article-element-place__title g-color-text"><?=($E->fields->name)?></p>
					<p class="b-article-element-place__description"><?=($E->fields->description)?></p>
				</td>
			</tr>
			<tr>
				<td class="b-article-element-place__show-on-map-holder">
					<? if (isset($E->fields->key) && $E->fields->key) {?>
					<div class="b-article-element b-article-element-show-on-map">
						<table class="b-article-element-show-on-map__layout">
							<tbody>
								<tr>
									<td width="110">
										<a href="javascript:void(0)" class="guidgmap_show b-article-element-show-on-map__icon" data-key="<?=$E->fields->key?>"></a>
									</td>
									<td>
										<a href="javascript:void(0)" class="guidgmap_show g-font-small-1 g-color-text b-article-element-show-on-map__title" data-key="<?=$E->fields->key?>">Посмотреть<br />на карте</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<?}?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
}
?>
