<?
class ElementGallery extends Element {

	protected $elementType = 'gallery';
	protected $title = 'Галерея';
	protected $editMode = 'basic';	
	
	public $galleriesList;
	public $currentGallery;
	
	public function __construct()
	{
		parent::__construct();
		#$this->getCurrentGallery();
		#var_dump($this );
	}
	
	protected function beforeRender()
	{
		//Для админки необходимо выбирать список галерей
		if($this->renderMode == 'admin')
			$this->getGalleriesList();	

		//Для предпросмотра в админке выбираем конкретную галерею
		if($this->renderMode == 'admin_preview')
			$this->getCurrentGallery();
	}

	
	private function getCurrentGallery()
	{

		if(isset($this->fields->gallery_id))
		{	
			$gallery = Galleries::Model()->with('photos')->findByPk($this->fields->gallery_id);

			$this->currentGallery = $gallery;
		}

	}

	private function getGalleriesList()
	{
		//Список галерей
		$galleries = Galleries::Model()->order('id DESC')->findAll();		
		
		$result_galleries[] = '--';
		//Приводим к виду ключ-значение
		foreach($galleries as $g)
		{
			$result_galleries[$g->id] = '['.$g->id.'] '.$g->name;
		}
		
		$this->galleriesList = $result_galleries;
	}	
}
?>