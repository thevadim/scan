<?
class ElementPoll extends Element {	

	protected $elementType = 'poll';
	protected $title = 'Опрос';
	protected $editMode = 'basic';	
	
	public $pollsList;
	
	public function __construct()
	{
		parent::__construct();
		$this->getPollList();
	}
	
	private function getPollList()
	{
		//Список голосований для элемента poll
		$polls = Polls::Model()->visible()->order('id DESC')->findAll();		
		
		$result_polls[] = '--';
		//Приводим к виду ключ-значение
		foreach($polls as $p)
		{
			/*if(!isset($max_strlen))
				$max_strlen = strlen($p->id);
				
			$nbsp_count = $max_strlen - strlen($p->id);	*/
				
			$result_polls[$p->id] = '['.$p->id.'] '.$p->name;
		}
		
		$this->pollsList = $result_polls;
	}		
	

}
?>