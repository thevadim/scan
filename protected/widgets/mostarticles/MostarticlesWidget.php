<?
/**
 * Виджет самых популярных и самых комментируемых статей
 */
class MostarticlesWidget extends CWidget
{
	//По просмотрам или количеству комментов
	public $type = 'by_views'; //by_views или by_comments
	public $days = 0;
	public $title = '';
	public $limit = 3;

	public function init() { }

	public function run()
	{
		$this->setDays();

		$cache_id = 'MostarticlesWidget.' . $this->type . $this->days;

		//Ищем в кеше
		$items = Yii::app()->cache->get($cache_id);

		if (!($items))
		{
			// Самое просматриваемое
			if ($this->type == 'by_views')
			{
				$items = $this->byViews();
			}

			// Самое обсуждаемое
			elseif ($this->type == 'by_comments')
			{
				$items = $this->byComments();
			}

			// Читайте также
			elseif ($this->type == 'by_readalso')
			{
				$items = $this->readAlso();
			}

			// Лучшие статьи
			elseif ($this->type == 'by_best')
			{
				$items = $this->best();
			}
			ArticlesPk::add($items);
			Yii::app()->cache->set($cache_id, $items, 60);
		}

		if (isset($items))
		{
			//Вытаскиваем статистику для выбранных статей
			$stat = ViewsFull::model()->getStatistic4Elements($items);
			$this->render('index', array('items' => $items, 'stat' => $stat));
		}
	}


	// Лучшие статьи
	private function best()
	{
		return Articles::model()->Indexed()->visible()->with('tree')->isMainAnnounce()->pkNotIn(ArticlesPk::getArray())->recently(3)->findAll();
	}


	// Самое читаемое
	private function byViews()
	{
		//Получим идентификаторы статей
		$sql = "SELECT a.id
				FROM bg_views_full v
				JOIN bg_articles a ON a.id=v.element_id
				WHERE a.date_active_start >= CURDATE() - INTERVAL :days DAY AND a.active=1
				ORDER BY v.views DESC
				LIMIT " . ($this->limit + 1);

		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":days", $this->days, PDO::PARAM_INT);
		$bvitems = $command->queryAll();
		$ids     = array();

		foreach ($bvitems as $bv)
			$ids[] = $bv['id'];

		$items = Articles::model()->Indexed()->visible()->with('tree')->findAllByPk($ids);
		return $items;
	}


	// Самое обсуждаемое
	private function byComments()
	{
		return false;
		$items = Articles::model()->Indexed()->visible()->mostCommented(3, 2)->with('tree')->findAll();
		return $items;
	}


	// Читайте также
	private function readAlso()
	{
		$parent_id = isset(Yii::app()->controller->parent_id) ? Yii::app()->controller->parent_id : 0;
		return Articles::model()->with('tree')->Indexed()->recently($this->limit + 1)->parentNotIs($parent_id)->visible()->isMiniNews(false)->pkNotIn(ArticlesPk::getArray())->findAll();
	}


	public function getTitle()
	{
		$titles = array(
			'by_readalso' => 'Читайте также',
			'by_comments' => 'Самое обсуждаемое',
			'by_best'        => 'Лучшие статьи',
			'by_views'    => 'Самое читаемое ' . $this->days
		);

		if ($this->title)
		{
			return $this->title;
		}
		if (isset($titles[$this->type]))
		{
			return $titles[$this->type];
		}
	}


	private function setDays()
	{
		// Если не установлено количество дней выборки, вычисляем по cookie
		if ($this->type == 'by_views' && $this->days == 0)
		{
			// Если установлена кука менее 7 дней назад
			if (isset($_COOKIE['lastvisit']) && !Yii::app()->session['show_article_last_month'])
			{
				$this->days = 7;
			}
			else
			{
				$this->days                                    = 30;
				Yii::app()->session['show_article_last_month'] = 1;
			}
			setcookie('lastvisit', time(), time() + 3600 * 24 * 7, '/');
		}
	}
}