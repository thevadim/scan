<?/**
 * @var LikesWidget $this
 */

$hide_counters_param = $this->show_counters == true ? '' : 'data-counters="no"';
$url_param = $this->url ? "data-url={$this->url}" : '';

?>
<div class="social-likes social-likes_<?=$this->look ?> social-likes_light" data-single-title="Поделиться" <?=$hide_counters_param?> <?=$url_param ?>>
	<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
	<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
	<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
	<div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
</div>