<?
/*
Получает идентификатор раздела в дереве и строит по нему путь
*/
class Breadcrumbs extends CWidget
{
	//Если передается идентификатор дерева
	public $tree_node_id = 0;
	
	//Если массив задается вручную
	//Массив вида $path[] = array('name' => 'Сюжеты', 'url' => '/story/')
	public $path = null;
	
	
	public function init()
	{
	}

	public function run()
	{		
		//Выбираем всех предков для хлебных крошек и прочего
    	if ($this->tree_node_id > 0)
		{
            /*$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::CACHE_KEY_BREADCRUMBS_WIDGET, array(), $this->tree_node_id);

            $this->path = Yii::app()->cache->get($CACHE_KEY);*/

            if($this->path === false)
            {
                $category = Tree::model()->findByPk($this->tree_node_id);

                if (isset($category))
                {
                    $descendants = $category->ancestors();
                    if (isset($descendants))
                    {
                        $descendants = $descendants->findAll();
                        foreach($descendants as $de)
                        {
                            if($de->clevel > 1)
                            {
                                $p['id'] = $de->id;
                                $p['code'] = $de->code;
                                $p['name'] = $de->name;
                                $p['url'] = '/'.$de->url;
                                $this->path[] = $p;
                            }
                        }
                    }
                    $p['id'] = $category->id;
                    $p['code'] = $category->code;
                    $p['name'] = $category->name;
                    $p['url'] = '/'.$category->url;
                    $this->path[] = $p;
                }

              /*  Yii::app()->cache->set($CACHE_KEY, $this->path, iMemCache::CACHE_DURATION_1_HOUR);*/
            }
		}

		$this->render('default', array('data' => $this->path));
	}
}
?>