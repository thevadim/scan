<?php
class SharesCountWidget extends CWidget
{
	public $url = null;
	
	public function run()
	{
		if (empty($this->url))
		{
			$this->url = Yii::app()->request->hostInfo . Yii::app()->request->url;
		}


		$CACHE_KEY = 'SharesCountWidget'.$this->url;
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{

			try {
				$ctx = stream_context_create(
					array(
						 'http' => array(
							 'timeout' => 0.5,
							 'ignore_errors' => true,
						 )
					)
				);

				$fb_res = @file_get_contents('https://graph.facebook.com/fql?q=' . urlencode('SELECT share_count FROM link_stat WHERE url=\'' . $this->url . '\''), false, $ctx);
				$vk_res = @file_get_contents('http://vk.com/share.php?act=count&index=1&format=json&url=' . $this->url, false, $ctx);
				$tw_res = @file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $this->url, false, $ctx);

				$_render['fb'] = @json_decode($fb_res)->data[0]->share_count;
				$_render['tw'] = @json_decode($tw_res)->count;
				$_render['vk'] = @preg_replace('/VK.Share.count\(\d+, (\d+)\);/i', '$1', $vk_res);
			}
			catch(Exception $e)
			{

			}

			Yii::app()->cache->set($CACHE_KEY, $_render, 60 * 10);
		}

		$this->render('sharescount', array('url' => $this->url, 'fb' => (int) $_render['fb'], 'vk' => (int) $_render['vk'], 'tw' => (int) $_render['tw']));
	}
}
