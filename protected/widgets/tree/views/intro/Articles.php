<div class="intro intro--dark" style="background-image: url(<?=$obj->preview_img?>);">
	<div class="wrapper wrapper-adaptive wrapper-relative">
		<div class="intro__text">
			<div class="intro__text-section">
				<?=$obj->tree->name?>
			</div>
			<div class="intro__text-title">
				<?=$obj->name?>
			</div>
			<?
			if ($obj->parent_id != 7)
			{
				?>
				<div class="intro__text-date">	
					<?= Yii::app()->dateFormatter->format('d MMMM в HH:mm', $obj->date_publish) ?>
				</div>
				<?
			}
			?>
		</div>
	</div>
</div>