<div class="b-article-element b-article-element_shiftable b-article-element-nav b-article-element-nav_pagination">
	<ul class="b-article-element-nav__paginator g-font-small-1">
		<?php
		$count         = count($chapters_pages);
		$count_visible = 5;
		$i             = 0;

		$start = $order_num - ceil($count_visible / 2) + 1;
		if ($start <= 0)
		{
			$start = 1;
		}
		if ($start + $count_visible <= $count)
		{
			$stop = $start + $count_visible - 1;
		}
		else
		{
			$stop  = $count;
			$start = $stop - $count_visible + 1;
		}
		if ($start <= 0)
		{
			$start = 1;
		}

		if ($start >= 2)
		{
			$start = $start + 1;
		}
		if ($count - $stop > 1)
		{
			$stop = $stop - 1;
		}

		foreach ($chapters_pages as $page)
		{
			$i++;
			if ($i >= $start && $i <= $stop || ($count == $i) || ($i == 1))
			{
				$link = $page == 1 ? $base_page : '?chapter=' . $page;
				if ($page == $order_num)
				{
					?>
					<li class="b-article-element-nav__paginator__page">
						<span class="g-color-text"><?=$page?></span>
						<em></em>
					</li>
					<?php
				}
				else
				{
					?>
					<?
					if ($i == $count && $stop + 1 < $count)
					{
						echo '<i>. . .</i><em></em>';
					}
					?>
					<li class="b-article-element-nav__paginator__page">

						<a href="<?=$link?>"><?=$page?></a>
						<em></em>
					</li>
					<?
					if ($i == 1 && $start >= 3)
					{
						echo '<i>. . .</i><em></em>';
					}
					?>
					<?php
				}
			}
		}
		?>
		<li class="b-article-element-nav__paginator__all">
			<a href="<?=$base_page?>?all">На одной странице</a>
			<i></i>
		</li>
	</ul>
</div>