# config valid only for Capistrano 3.1
#lock '3.2.1'
lock '3.3.5'

set :ssh_options, { :forward_agent => true }
set :ssh_options, { :keys => [File.join(ENV["HOME"], ".ssh", "id_rsa")] }


set :application, 'scan'
set :repo_url, 'git@bitbucket.org:rbmd/scan.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/rbmd_deploy/forum.scan-interfax.ru'

set :deploy_via, :remote_cache

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{assets media upload protected/runtime }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "sudo /etc/init.d/php5-fpm restart"
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      within release_path do
      #   execute :rake, 'cache:clear'
        run "#{release_path}/protected/yiic migrate up --interactive=false"
      end
    end
  end

end
