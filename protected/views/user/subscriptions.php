<h1>Подписки пользователя</h1>

<? $this->widget('application.widgets.profile.summary.UserSummaryWidget',
				array(
					'user_id' => $user->id,
				)
			); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.userMenu.UserMenuWidget',
				array(
					'user_id' => $user->id,
				)
			);
	?>
	
	<?
	if (count($subscriptions) > 0)
	{
		?>
		<div class="tabwrap">
			<div id="tab-subts" class="box _optional">
				<table class="blogTb">
					<tbody>
						<?
						foreach ($subscriptions as $i => $s)
						{
							if (~ $i & 1)
							{
								?><tr><?
							}
							?>
								<td>
									<img src="<?= ThumbsMaster::getThumb($s->Author->avatar, ThumbsMaster::$settings['avatar_81'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="">
								</td>
								<td>
									<?
									if (!empty($s->Author->blog))
									{
										?>
										<span class="login"><a href="/user/<?= $s->Author->id ?>/"><?= $s->Author->blog->code ?></a></span> <span>(<?= $s->Author->lastname ?> <?= $s->Author->firstname ?>)</span>
										<span class="_grey"><?= $s->Author->profession ?></span>
										<?
									}
									else
									{
										?>
										<span class="login"><span><?= $s->Author->lastname ?> <?= $s->Author->firstname ?></span>
										<span class="_grey"><?= $s->Author->profession ?></span>
										<?
									}
									?>
								</td>
							<?
							if ($i & 1)
							{
								?></tr><?
							}
						}

						if (~ $i & 1)
						{
							?></tr><?
						}
						?>
					</tbody>
				</table>
			</div>

		</div>
		<?
	}
	else
	{
		?>
		<p>У пользователя нет ни одной подписки</p>
		<?
	}
	?>
</div>
