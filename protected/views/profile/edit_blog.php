<h1>Редактирование блога</h1>

<?
CHtml::$errorCss = '';
$form = $this->beginWidget('CActiveForm', 
							array(
									'action' => '/my/blog/',
									'id'=>'profile-form',
									'enableClientValidation'=>true,
									'enableAjaxValidation'=>true,
									'clientOptions' => array(
										'validateOnSubmit' => true,
										'validateOnChange' => true,
										'errorCssClass' => 'error',
										'inputContainer' => 'div.row',
										'afterValidate' => 'js:function(form, data, hasError) { 
											if (hasError) {
												for(var i in data) $("#"+i).closest(".row").addClass("error");
												return false;
											}
											else {
												return true;
											}
										}',
										'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
											if (hasError) $("#"+attribute.id).closest(".row").addClass("error");
										}'
									)
								)
							);
?>
<div class="sign-up-form">
	<div class="form-wrapper form-wrapper-wide">

		<div class="row">
			<label><?= $model->attributeLabels()['blog_code'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?
				
				{
					echo $form->textField($model, 'blog_code', array('value' => $model->blog->code, 'class' => 'field', 'onkeyup' => "$('#blog_path').text( $(this).val() + ($(this).val().length > 0 ? \"/\" : \"\") )"));
					?>
					<?= $form->error($model, 'blog_code', array('class' => 'error error-wide')) ?>
					<div>
						<br />
						<?= Yii::app()->request->hostInfo . '/blogs/<span id="blog_path">' . $model->blog->code . '</span>' ?>
					</div>
					<?
				}
				?>
			</span>
		</div>

		<div class="row">
			<label><?= $model->attributeLabels()['blog_name'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'blog_name', array('value' => $model->blog->name, 'class' => 'field')) ?>
				<?= $form->error($model, 'blog_name', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label class="label-top"><?= $model->attributeLabels()['blog_description'] ?></label>
			<span class="field-wrapper">
				<?= $form->textArea($model, 'blog_description', array('value' => $model->blog->description, 'class' => 'field field-wide')) ?>
				<?= $form->error($model, 'blog_description', array('class' => 'error error-wide')) ?>
			</span>
		</div>

	</div>


	<?= CHtml::submitButton('Сохранить', array('class' => 'button _blue-button')) ?>
</div>

<?
$this->endWidget();
?>