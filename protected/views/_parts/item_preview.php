<?
// Item
$tmp = explode(' ', $item->name, 2);
if (count($tmp) > 1 && is_numeric($tmp[0])) {
	$title = '<span class="b-preview__item__title__first-num g-color-text">' . $tmp[0] . '</span> ' . $tmp[1];
} else {
	$title = $item->name;
}

/*
$title = preg_replace_callback(
	'~^(\d+)(\s[\s\S]+)$~',
	function ($match) {
		return '<span class="b-preview__item__title__first-num g-color-text">' . $match[1] . '</span>' . $match[2];
	},
	$item->name
);
*/

switch ($item->tree->id) {
	case 8: $type = 'guide'; break;
	default: $type = 'article'; break;
}
if ($item->is_guide == 1) {
	$type = 'guide';
}
if ($item->is_howto == 1) {
	$type = 'howto';
}
if (isset($isMagazine) && $isMagazine == 1) {
	$type = 'magazine';
}


//print_r($item->tree);

$previewClass = 'b-preview b-preview_size_large b-preview_type_'.$type.' g-color-'.$type.'';
?><div class="<?=$previewClass?>">
	<table class="b-preview__head">
		<tbody>
			<tr>
				<td class="b-preview__head__left">
					<a href="/<?=$item->tree->url?>" class="g-font-small-2 g-color-text-hover"><?php if ($item->is_archive) : ?><i class="g-icon g-icon_archive g-icon_archive_hovered"></i> <?php endif; ?><?=$item->tree->name?>&nbsp;&nbsp;<i class="g-icon g-icon_triangle_right"></i></a>
				</td>
				<td class="b-preview__head__right">
					<span class="g-font-small-1 b-preview__head__publish-date"><?=UtilsHelper::timeToNiceString($item->date_publish)?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="b-preview__item g-color-border">
		<?php
		$extra_params = json_decode($item->extra_params, true); ?>

		<span class="g-margin-uncollapse"></span>
		<a href="<?=$item->url()?>" class="g-color-text-hover">
    <?php
  		if ($item->radiosvoboda == 1)
  		{
  			?>
  			<div class="main-radiosvoboda">
  				<img src="/static/images/pub/specprojects/radiosvoboda/svoboda.png" alt="радио свобода"/>
  			</div>
  		<? }
  		elseif ($item->theme_week_code)
  		{
  			?>
  			<div class="theme-week">
  				<a href="/themeweek/<?=$item->theme_week_code?>/"><img src="http://bg.ru/media/theme_week/<?=$item->theme_week_code?>/circle.png" alt=""/></a>
  			</div>
  		<?
  		}
  		elseif (strpos($item->link, 'blogs/'))
  		{
  			?>
  			<div class="b-preview__badge b-preview__badge_blogs"></div>
    <?
  		}
  		elseif (strpos($item->link, 'bestsummer2013/'))
  		{
  			?>
  			<div class="b-preview__badge b-preview__badge_bestsummer"></div>
  		<?
  		}
  		elseif (isset($extra_params['blogs_digest']))
  		{
  			?>
  			<div class="article-blogs-digest"></div>
  		<?
  		}
      elseif (isset($extra_params['cityboom']))
      {
        ?>
        <div class="b-preview__badge b-preview__badge_cityboom"></div>
      <?
      }
      ?>
			<?php
				/* 0 -> photo, 1 -> quote */
				if ($item->ceil_type == 1) {
			?>
				<p class="b-preview__item__quote">
					<span class="b-preview__item__quote__author g-font-title-2"><?=$item->ceil_quote_author?>:</span>
					<span class="b-preview__item__quote__text"><?=$item->ceil_quote_text?></span>
				</p>
			<?php } else { ?>
				<img src="<?=$item->img('320x180')?>" width="300" alt="<?=strip_tags($item->name)?>" title="<?=strip_tags($item->preview_text)?>" class="b-preview__item__image" />
				<p class="b-preview__item__title g-font-title-2"><?=$title?></p>
			<?php } ?>
			<?php if ($item->ceil_type != 1) { ?>
			<p class="b-preview__item__subtitle"><?=$item->preview_text?></p>
			<?php } ?>
		</a>
		<ul class="b-preview__item__meta b-meta g-font-small-1">
			<?Yii::app()->controller->renderPartial('application.views.layouts.parts.article_info_block', array('article' => $item,'stat' => $stat))?>
		</ul>
		<a href="<?=$item->url()?>" class="b-preview__item__arrow g-icon g-icon_arrow_right"></a>
	</div>
</div><?php ?>