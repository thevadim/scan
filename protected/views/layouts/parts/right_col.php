<div class="col right">
    <div class="extender"></div>
    <div class="inner">
        <div class="webinars">
            <div class="caption">
                Вебинары
            </div>

            <?
            if(strlen($this->webinar['webinar_id']) > 0){?>
                <div id="mini-player" data-id="<?=$this->webinar['webinar_id']?>"></div>
                <a href class="expand-player">Развернуть</a>
            <?} else {?>
                <div class="announce" style="background-image: url(<?=$this->webinar['announce_pic']?>)">
                    <div class="date">
                        <? if ($this->webinar['announce_day']
                            && $this->webinar['announce_month']
                            && $this->webinar['announce_time']): ?>
                        <div>
                                <div class="day"><?=$this->webinar['announce_day']?></div>
                                <div class="month">/<?=$this->webinar['announce_month']?></div>
                        </div>
                        <div class="time"><?=$this->webinar['announce_time']?></div>
                        <?php else: ?>
                            <div class="day">СКОРО</div>
                        <? endif; ?>
                    </div>
                    <div class="overlay">
                        <div class="title">
                            <?=$this->webinar['announce_title']?>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <div class="banner banner-240-200">
            <!--					<img src="/img/banner_2.jpg" alt=""/>-->
            <object type="application/x-shockwave-flash" data="/files/ForexClub_Usa_240x200_rbc.swf" width="240" height="200">
                <param name="movie" value="/files/ForexClub_Usa_240x200_rbc.swf">
                <param name="quality" value="high">
            </object>
        </div>
<!--        <div class="banner banner-240-200">-->
<!--            <a href="">-->
<!--                <img src="/img/banner_3.jpg" alt=""/>-->
<!--            </a>-->
<!--        </div>-->
    </div>
</div>