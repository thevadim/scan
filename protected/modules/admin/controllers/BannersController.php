<?php
/**
 * User: Русаков Дмитрий
 * Date: 15.06.12
 * Time: 18:54
 *
 * Контроллер модуля для вставки баннеров на сайт
 *
 */

class BannersController extends Controller
{
	public $layout = 'index';

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		//Подрубаем css со свойствами для валидации полей формы создания баннера
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}

	/*Показать все баннеры*/
	public function actionIndex()
	{
		$criteria = new CDbCriteria();

		$pages = new CPagination( Banners::model()->count() );
		$pages->pageSize =20;
		$pages->applyLimit($criteria);

		$banners = Banners::model()->sort_by_id_desc()->findAll($criteria);

		$this->render(
			'index',
			array('banners'=>$banners, 'pages'=>$pages)
		);
	}

	/*
		 * Добавление/редактирование нового баннера
		 */
	public function actionEdit($id = 0)
	{
		$new_flag = false;

		$model = Banners::model()->findByPk($id);

		if( empty($model) )
		{
			$model = new Banners();
			$new_flag = true;
		}

		//Выдать сообщения об ошибке в случае аякс валидации
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'banner-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		if( isset($_POST['Banners']) )
		{
			$model->attributes = $_POST['Banners'];

			if( $model->validate() )
			{
				$model->save();

				if( $new_flag )
				{
					$this->redirect("/admin/banners/");
				}
				else
				{
					if( !isset($_POST['apply']) )
					{
						$this->redirect("/admin/banners/");
					}
					else{
						$this->refresh();
					}
				}
			}
		}

		$this->render(
			'edit',
			array('model' => $model, 'is_new' => $new_flag)
		);
	}

    /*
    * Удаление баннера
    */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Banners::model()->dbConnection->beginTransaction();

                try
                {
                    Banners::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/banners/');
    }
}
