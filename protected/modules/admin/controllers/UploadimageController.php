<?php
class UploadImageController extends Controller
{
	public $maxWidth = 1200;
	public $maxHeight = 1200;

	public function actionIndex($module = null, $controller = null, $id = null)
	{

		$path = $this->getPath($module, $controller, $id);
		$ext  = substr($_FILES['file']['type'], strpos($_FILES['file']['type'], '/') + 1);
		$ext  = ($ext == 'jpeg') ? 'jpg' : $ext;

		$filename = $path . $this->getFileName($path) . '.' . $ext;

		$image = new Images($_FILES['file']['tmp_name']);
		if (($image->width() > $this->maxWidth || $image->height() > $this->maxHeight) && $ext != 'gif')
		{
			$image->resize($this->maxWidth, $this->maxHeight);
		}
		$image->save($filename, $ext, 80);
		echo '{"filelink":"/' . $filename . '"}';
	}


	// Получаем директорию
	public function getPath($module, $controller, $id)
	{
		$aPath[] = 'media/redactor';
		if ($module)
		{
			$aPath[] = $module;
		}
		if ($controller)
		{
			$aPath[] = $controller;
		}
		if ($id)
		{
			$aPath[] = implode('/', str_split($id));
		}
		if (count($aPath) == 1)
		{
			$aPath[] = date('Y/m/d');
		}
		return implode('/', $aPath) . '/';
	}

	// Получить имя файла без расширения для переданной директории
	public function getFileName($path)
	{
		if (is_dir($path))
		{
			return intval(scandir($path, SCANDIR_SORT_DESCENDING)[0]) + 1 . '_' . substr(md5(microtime()), 0, 5);
		}
		return 1;
	}

}