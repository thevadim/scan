<?php
class AuthorsController extends Controller
{
	public $layout = 'index';

	//id раздела в дереве, к которому привязываются типы авторов
	private $tree_id = 1866;

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		//Подрубаем css со свойствами для валидации полей формы создания лекции
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}

	public function actionIndex()
	{
		$criteria = new CDbCriteria();		
		$criteria->order = 't.surname ASC';
		$criteria->select = 't.*';

        $id = Yii::app()->request->getParam('id', null);
        $name = Yii::app()->request->getParam('name', null);
        $surname = Yii::app()->request->getParam('surname', null);

        if($id)
        {
            $criteria->addCondition('t.id = ' . (int)$id);
        }

        if($name)
        {
            $criteria->addSearchCondition('t.name', urldecode($name));
        }

        if($surname)
        {
            $criteria->addSearchCondition('t.surname', urldecode($surname));
        }

		$count = Authors::model()->count($criteria);

		$pages = new CPagination($count);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);	

		$authors = Authors::model()->findAll($criteria);

		$this->render('index', array('authors' => $authors, 'pages' => $pages) ); 
	}

	public function actionCreate()
	{
		$model = new Authors();

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'authors-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		if(isset($_POST['Authors']))
		{		
			$model->attributes = $_POST['Authors'];
			$model->selected_types = isset($_POST['Authors']['types']) ? $_POST['Authors']['types'] : null;
			#var_dump();
			#exit;
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/authors/");
				else
					$this->redirect('/admin/authors/edit/?id='.$model->id, true);
			}

		}
		$t = $this->getTree();

		$this->render('edit', array('model' => $model, 'action' => 'create', 'tree' => $t) );
	}

	public function actionEdit($id)
	{
		$model = Authors::model()->FindByPk($id+0);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/wysiwyg_manager.js'), CClientScript::POS_END);
		if ( empty($model) )
			throw new CHttpException(404);

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'authors-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		if (isset($_POST['Authors']))
		{
			if (!isset($_POST['Authors']['user_id']))
				$_POST['Authors']['user_id'] = null;
			
			$model->attributes = $_POST['Authors'];
			$model->selected_types = isset($_POST['Authors']['types']) ? $_POST['Authors']['types'] : null;

			if( $model->validate() )
			{
				$model->save();

				if( !isset($_POST['apply']) )
				{
					$this->redirect("/admin/authors/");
				}
				else
				{
					$this->refresh();
				}
			}						
		}

		$t = $this->getTree();
		$this->render('edit', array('model' => $model, 'action' => 'edit', 'tree' => $t ) );
	}	

	//Выбирает из дерева разделы, к которым привязываются ведущие
	private function getTree()
	{
		return false;
		//Выбираем только подразделы ведущих
		$p = Tree::model()->findByPk($this->tree_id);
		return $p->descendants()->findAll();		
	}

	public function actionGetAuthors($filter = '', $attachToModel = '', $variableName = '', $type = 'authors')
	{
		$filter = urldecode($filter);
		//if (Yii::app()->request->isAjaxRequest) 
		{
			$criteria = new CDbCriteria();
			if (strlen($filter) > 0) {
				$criteria->condition = 'id = :id OR name LIKE :name';
				$criteria->params = array(':id' => $filter, ':name' => '%'.$filter.'%');
			}
			$criteria->order = 'id DESC';

			$count = Authors::model()->count($criteria);

			$pages = new CPagination($count);
			$pages->pageSize = 5;
			$pages->applyLimit($criteria);

			$output = array();
			$authors_all = Authors::model()->findAll($criteria);
			foreach ($authors_all as $a)
			{
				$output[] = array('id' => $a->id, 'name' => $a->name );
			}

			$this->layout = 'ajax';
			$view = $type == 'authors' ? 'getauthors' : 'getpresenters';
			$this->render(
							$view,
							array(
								'output' => $output,
								'filter' => $filter,
								'attachToModel' => $attachToModel,
								'variableName' => $variableName,
								'pages' => $pages,
							)
						);
		}
	}
	
	//Возвращает json массив по введенным имени или фамилии(или по первым буквам, не менее двух)
	public function actionGetByName($s)
	{

		$q = new CDbCriteria();
		$q->compare('LOWER(name)',strtolower($s),true); 
		$q->compare('LOWER(surname)',strtolower($s),true, 'OR'); 

		$q->limit = 50;
		
		$authors = Authors::model()->findAll($q);
		
		$i = 0;
		$arr = array();
		foreach($authors as $author)
		{
			$arr[$i]['key'] = $author->id;
			$arr[$i]['value'] = $author->fullname();
			$i++;
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}

	public function actionImageSelect($variableName = '')
	{
		//if (Yii::app()->request->isAjaxRequest || true) 
		{
			require_once $_SERVER['DOCUMENT_ROOT'].'/ckfinder/ckfinder.php' ;

			$finder = new CKFinder() ;
			$finder->BasePath = '/ckfinder/' ;
			$finder->Height = 500;

			$this->layout = 'ajax';
			$this->render(
							'imageselect',
							array(
								'finder' => $finder,
								'variableName' => $variableName,
							)
						);
		}
	}

	/*
	* Удаление автора
	*/
	public function actionDelete()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$id = Yii::app()->request->getParam('id');

			if( !empty($id) )
			{
				$transaction = Authors::model()->dbConnection->beginTransaction();

				try{
					ArticlesToAuthors::model()->deleteAll('author_id=:id', array(':id'=>$id));
					Authors::model()->findByPk($id)->delete();

					$transaction->commit();
				}
				catch(Exception $e)
				{
					$transaction->rollBack();

					throw $e;
				}
			}

			Yii::app()->end();
		}

		$this->redirect('/admin/authors');
	}
}