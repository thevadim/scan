<?php

class RubricsController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'year DESC, month DESC';
		
		$articles = LikbezRubrics::model()->findAll($criteria);

		

		$this->render('index', array('articles' => $articles, ) );
	}

	public function actionArticles()
	{
		

		$this->render('articles', array('articles' => $articles) );
	}
	
	public function actionCreate()
	{
		$model = new LikbezRubrics();
		
		if(isset($_POST['LikbezRubrics']))
		{			
			$model->attributes = $_POST['LikbezRubrics'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/likbez/rubrics/");
				else
					$this->redirect('/admin/likbez/rubrics/edit/?id='.$model->id, true);
			}

		}
	
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = LikbezRubrics::model()->FindByPk($id);
			
			if(isset($_POST['LikbezRubrics']))
			{
				$model->attributes = $_POST['LikbezRubrics'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/likbez/rubrics/');
                    }
                    else
					{
                        $this->redirect('/admin/likbez/rubrics/edit/?id='.$model->id, true);
                    }
				}
			}
			
			

			$this->render('edit', array('model' => $model, 'action' => 'edit'  ) );		
		}
	}


    /*
     * Удаление тега
     */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = LikbezRubrics::model()->dbConnection->beginTransaction();

                try{
                    
                    LikbezRubrics::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/likbez/rubrics/');
    }
}