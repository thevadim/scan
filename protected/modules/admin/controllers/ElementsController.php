<?php

class ElementsController extends Controller
{
	//Обязательно переопределяем методы для того, чтобы корректно возвращался аякс
	protected function afterRender($view, &$output)
	{
		return true;
	}

	//Используется в админке при добавлении и редактировании элементов
	public function actionGetElementForm($element_type = null, $element_id = null, $mode = 'admin', $chapter_id = null)
	{
		$params['elementType']   = $element_type;
		$params['elementId']     = $element_id;
		$params['chapterId']     = $chapter_id;
		$params['renderMode']    = $mode;
		$params['refreshFromDb'] = true;

		//Если установлен chapter_id, значит создается новый элемент
		if ($chapter_id && is_null($element_id))
		{
			$params['create'] = true;
		}

		$e = ElementBuilder::build($params);
		$e->render();

	}

	//Сохранение элемента
	public function actionSave($id, $type)
	{
		$params['elementType'] = $type;
		$params['elementId']   = $id;
		$params['renderMode']   = 'admin_preview';
		$e                     = ElementBuilder::build($params);
		$e->setContent($_POST[$id], true);

		$e->save();	
			
		$e->render();

	}

	//Сохранение inline-элемента
	public function actionSaveInline($id, $type)
	{
		if (empty($_POST))
		{
			exit;
		}

		// TODO Задача #575. Решить на стороне CKEditor
		$_POST['text'] = str_replace('class="data-cke-saved-name" ', '', $_POST['text']);
		$_POST['text'] = preg_replace('#data-cke-saved-name#', 'class="data-cke-saved-name" data-cke-saved-name', $_POST['text']);


		$params['elementType'] = $type;
		$params['elementId']   = $id;
		$e                     = ElementBuilder::build($params);
		$e->setContent($_POST, true);
		$e->save();
	}

	// Сортировка элементов в главе
	public function actionSortable()
	{
		$ar = $_POST['list'];
		$i  = 1;
		if (is_array($ar))
		{
			foreach ($ar as $id)
			{
				if ($id > 0)
				{
					ElementsDraft::model()->updateByPk($id, array('order_num' => $i));
					$i++;
				}
			}
		}
	}


	// Удаление элемента
	public function actionRemove($id)
	{
		echo ElementsDraft::model()->deleteByPk(intval($id));
		exit;
	}


	//Отдает уникальный ключ при добавлении элемента на карту гидов
	public function getKeyByTime()
	{
		return md5(round(microtime(true) * 1000));
	}

	//Отдает уникальный ключ при добавлении элемента на карту гидов
	public function actionGetGMapDot()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			//Добавляем стандартную точку с центровкой в кремле
			$dot['key']   = $this->getKeyByTime();
			$dot['title'] = 'Кремль';
			$dot['x']     = '55.7544745';
			$dot['y']     = '37.61784339999997';

			$view = $this->renderPartial('application.modules.admin.views.articles.parts.gmap_dot', array('is_new' => 1, 'dot' => $dot), true);

			$json = array('view' => $view, 'key' => $dot['key'], 'title' => $dot['title'], 'x' => $dot['x'], 'y' => $dot['y']);
			echo CJSON::encode($json);
		}
	}

}