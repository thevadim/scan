<?php

class GalleryController extends Controller 
{
	public $layout = 'index';
	public $thumb_sizes = array(160, 320, 640);
	public $gallery_path = '';

	public function __construct($id,$module=null) {
		$this->gallery_path = Yii::app()->getBasePath() . '/../media/galleries/660x440';
		parent::__construct($id,$module);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/wysiwyg_manager.js'), CClientScript::POS_END);
      Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/gallery/script.js'), CClientScript::POS_END);
	}

	public function actionIndex()
	{
		$criteria = new CDbCriteria();

		$id   = Yii::app()->request->getParam('id', null);
		$name = Yii::app()->request->getParam('name', null);

		if ($id)
		{
			$criteria->addCondition('t.id = ' . (int)$id);
		}

		if ($name)
		{
			$criteria->addSearchCondition('t.name', urldecode($name));
		}

		$criteria->order = 't.id DESC';
		$criteria->select = 't.*';

		$count = Galleries::model()->count($criteria);
	 
		$pages = new CPagination($count);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);	

		$Galleries = Galleries::model()->findAll($criteria);
		
		$this->render( 'index', array(
			'Galleries' => $Galleries,
			'pages' => $pages
		));
	}

	/*
	 * Создание или редактирование галереи
	 *
	**/
	public function actionSave() {
		if (Yii::app()->request->isAjaxRequest && isset($_POST['Gallery'])) {
			if (isset($_POST['Gallery']['id'])) {
				$Gallery = Galleries::model()->findByPk( (int)$_POST['Gallery']['id'] );
			} else {
				$Gallery = new Galleries;
			}
			$Gallery->name = $_POST['Gallery']['name'];
			$transaction = Galleries::model()->dbConnection->beginTransaction();
			try {
				if (!$Gallery->save()) {
					echo json_encode($Gallery->getErrors());
					throw new Exception;
				}
				if (isset($_POST['Gallery']['photos'])) {
					$i = 0;
					foreach ($_POST['Gallery']['photos'] as $photo) {
						$i++;
						if (isset($photo['id'])) {
							$GalleryPhoto = GalleryPhotos::model()->findByPk( (int)$photo['id'] );
						} else {
							$GalleryPhoto = new GalleryPhotos;
						}

		 				$GalleryPhoto->gallery_id = $Gallery->id;
		 				$GalleryPhoto->order_num = $i;
						$GalleryPhoto->path = $photo['filedir'];
						//$GalleryPhoto->name = $photo['name'];
						$GalleryPhoto->alt = $photo['alt'];
						$GalleryPhoto->copyright = $photo['copyright'];
						$GalleryPhoto->detail_text = $photo['detail_text'];

						if (!$GalleryPhoto->save()) {
							echo json_encode($GalleryPhoto->getErrors());
							throw new Exception;
						}
					}
				}

				if ($Gallery->id) {
					if ($_POST['Gallery']['actionType'] == 'save') {
						echo json_encode(array('redirect' => '/admin/gallery'));
					} elseif ($_POST['Gallery']['actionType'] == 'apply') {
						echo json_encode(array('redirect' => "/admin/gallery/edit/id/{$Gallery->id}/"));
					}
				} else {
					throw new Exception;
				}

				$transaction->commit();
			} catch (Exception $e) {
				$transaction->rollback();
				throw $e;
			}
			
			Yii::app()->end();
		}
	}

	public function actionDelete($id) {
		$id = (int)$id;
		if ($id) {
			$Photos = GalleryPhotos::model()->findAll(
				'gallery_id=:gallery_id',
				array(':gallery_id' => $id)
			);
			if (count($Photos)) {
				foreach ($Photos as $photo) {
					$photo->delete();
				}
			}
			
			if (Galleries::model()->deleteByPk($id)) {
				UtilsHelper::removeDirectory($this->gallery_path . '/' . $id);
				$this->redirect('/admin/gallery/');
			}

		}
	}

	public function actionCreate() {

		//unlink(Yii::app()->getBasePath.'/../media/galleries/thumbs/02742012/');
		Yii::import("ext.xupload.models.XUploadForm");
		$this->render( 'create', array(
			'Galleries' => new Galleries(),
			'GalleryPhotos' => new GalleryPhotos(),
			'XUploadForm' => new XUploadForm
		));
	}

	public function actionEdit($id) {
		$id = (int)$id;
		if ($id) {
			Yii::import("ext.xupload.models.XUploadForm");
			$Gallery = Galleries::model()->findByPk($id);
			if (isset($Gallery)) {
				$this->render( 'edit', array(
					'Gallery' => Galleries::model()->findByPk($id),
					'XUploadForm' => new XUploadForm 
				));
			} else {
				throw new CHttpException(404);
			}
		} else {
			throw new CHttpException(404);
		}
	}

	public function actionForm( ) {
	    $model = new GalleryPhotos;
	    Yii::import( "ext.xupload.models.XUploadForm" );
	    $photos = new XUploadForm;
	    //Check if the form has been submitted
	    if( isset( $_POST['GalleryPhotos'] ) ) {
	        //Assign our safe attributes
	        $model->attributes = $_POST['GalleryPhotos'];
	        //Start a transaction in case something goes wrong
	        // $transaction = Yii::app( )->db->beginTransaction( );
	        // try {
	            //Save the model to the database
	            if($model->save()){
	                // $transaction->commit();
	            }
	        // } catch(Exception $e) {
	        //     $transaction->rollback( );
	        //     Yii::app( )->handleException( $e );
	        // }
	    }
	    $this->render( 'uploadPhotos', array(
	        'model' => $model,
	        'photos' => $photos,
	    ) );
	}


	public function actionDeleteImage($id = 0)
	{
		$photo = GalleryPhotos::model()->findByPk($id);
		if ($photo->delete())
		{
			$path = $this->gallery_path . $photo->path;
			if (is_file($path))
			{
				unlink($path);
				echo 'done';
			}
			else echo 'not exist file ' . $path;
		}
		else echo 'error ';
	}

	public function actionUploadImages( ) {
	    Yii::import( "ext.xupload.models.XUploadForm" );
		$path = Yii::getPathOfAlias('webroot') . '/assets/upload/';
		if (!is_dir($path))
		{
			mkdir($path, 0775, true);
			chmod($path, 0775);
		}

	    //This is for IE which doens't handle 'Content-type: application/json' correctly
	    header( 'Vary: Accept' );
	    if( isset( $_SERVER['HTTP_ACCEPT'] ) 
	        && (strpos( $_SERVER['HTTP_ACCEPT'], 'application/json' ) !== false) ) {
	        header( 'Content-type: application/json' );
	    } else {
	        header( 'Content-type: text/plain' );
	    }

        $model = new XUploadForm;
        $model->file = CUploadedFile::getInstance( $model, 'file' );
        //We check that the file was successfully uploaded
        if( $model->file !== null ) {
            //Grab some data
            $model->mime_type = $model->file->getType( );
            $model->size = $model->file->getSize( );
            $model->name = $model->file->getName( );
            //(optional) Generate a random name for our file 
            $basename = md5( Yii::app( )->user->id.microtime( ).$model->name);
            $extension = $model->file->getExtensionName();
            $filename = mb_strtolower($basename.".".$extension);
            $model->id = Yii::app( )->request->getQuery( null, date( "mdY" ) );
            // var_dump($this->gallery_id);
            // die();
            if( $model->validate( ) ) { 

                $tmp_dir = $path.$model->id.'/';
                if (!file_exists($tmp_dir)) mkdir($tmp_dir, 0777);
                // $tmp_dir = Yii::getPathOfAlias('webroot') . "/tmp2/";
                
                $model->file->saveAs( $tmp_dir.$filename );
                
                // Thumbnails 
 				
 
                //Now we need to save this path to the user's session
                if( Yii::app( )->user->hasState( 'images' ) ) {
                    $userImages = Yii::app( )->user->getState( 'images' );
                } else {
                    $userImages = array();
                }
                 $userImages[] = array(
                    "path" => $path.$filename,
                    //the same file or a thumb version that you generated
                    "thumb" => $path.$filename,
                    "filename" => $filename,
                    'size' => $model->size,
                    'mime' => $model->mime_type,
                    'name' => $model->name,
                );
                Yii::app( )->user->setState( 'images', $userImages );
 
                //Now we need to tell our widget that the upload was succesfull
                //We do so, using the json structure defined in
                // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                $url = '/assets/upload/'.$model->id;
                echo json_encode( array( array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => "{$url}/{$filename}",
                        "thumbnail_160_url" => "{$url}/{$filename}",
                        "dir" => "/{$model->id}/{$filename}",
                        "delete_url" => "/admin/gallery/deleteImage/subdir/{$model->id}/filename/{$filename}/",
                        "delete_type" => "POST"
                    ) ) );
                Yii::app()->end();
            } else {
                //If the upload failed for some reason we log some data and let the widget know
                echo json_encode( array( 
                    array( "error" => $model->getErrors( 'file' ),
                ) ) );
                Yii::log( "XUploadAction: ".CVarDumper::dumpAsString( $model->getErrors( ) ),
                    CLogger::LEVEL_ERROR, "ext.xupload.actions.XUploadAction" 
                );
            }
        } else {
            throw new CHttpException( 500, "Could not upload file" );
        }
	}

	//Показывает основу галереи - первую фотку, количество фоток
	public function actionPlace($id = null, $size = 'small')
	{
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::GALLERY_PLACE, array(), $id . $size);
		$_render   = Yii::app()->cache->get($CACHE_KEY);

		if ($_render == false)
		{
			$gallery = Galleries::model()->findByPk($id, array('select' => 'id,name'));

			//Общее количество фоток в галерее
			$criteria         = new CDbCriteria();
			$_render['count'] = GalleryPhotos::model()->galleryIdIs($id)->count($criteria);

			if (!is_null($gallery))
			{
				$photos = GalleryPhotos::model()->galleryIdIs($gallery->id)->findAll(array('order' => 'order_num ASC'));
				/*foreach ($photos as $photo)
				{
					if (!isset($first_photo))
					{
						$first_photo = $photo;
					}

					if ($size == 'large')
					{
						$photo_img_path = $photo->img('840x560');
					}
					else
					{
						$photo_img_path = $photo->img('660x440');
					}

					
					//$_render['photos_js'][] = array('img' => $photo_img_path, 'detail_text' => $photo->detail_text, 'copyright' => addslashes($photo->copyright));
				}*/
				$_render['photos'] = $photos;
				$_render['gallery']     = $gallery;
				//$_render['first_photo'] = $first_photo;
				Yii::app()->cache->set($CACHE_KEY, $_render, 60);
				Yii::app()->cache->addDependency($CACHE_KEY, array('Galleries' => array($id)));
			}
		}

		if (isset($_render['photos']))
		{

			$this->renderPartial('place', array('gallery' => $_render['gallery'], 'photos' => $_render['photos'], 'count' => $_render['count'], 'type' => $size));
		}
	}
}