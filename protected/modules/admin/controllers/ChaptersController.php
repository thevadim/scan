<?php

class ChaptersController extends Controller
{
	//Обязательно переопределяем методы для того, чтобы корректно возвращался аякс из $this->actionGetChapterForm()
	protected function afterRender($view, &$output)
	{
		return true;
	}

	//Метод рендерит виджет с задаными параметрами.
	//Используется в админке при добавлении глав
	public function actionGetChapterForm($article_id)
	{
		$article_id = intval($article_id);
		$article    = ArticlesDraft::model()->find('article_id=:id AND is_dummy=1', array('id' => $article_id));

		$new_Chapter                   = new ChaptersDraft();
		$new_Chapter->order_num        = 1 + ChaptersDraft::model()->count('article_draft_id=:id AND hist=0', array('id' => $article->id));
		$new_Chapter->name             = 'Новая глава';
		$new_Chapter->hist             = 0;
		$new_Chapter->article_id       = $article->article_id;
		$new_Chapter->article_draft_id = $article->id;
		$new_Chapter->save();
		$this->render('getchapterform', array('chapters' => array($new_Chapter)));
	}


	// Сохранение данных главы
	public function actionSave()
	{
		$chapter_id     = intval($_POST['chapter_id']);
		$name           = $_POST['name'];
		$value          = $_POST['value'];
		$chapter        = ChaptersDraft::model()->findByPk($chapter_id);
		$chapter->$name = $value;
		$chapter->save();
	}


	// Удаление главы
	public function actionRemove($id)
	{
		$id = intval($id);
		ElementsDraft::model()->deleteAll('chapter_id=:chapter_id AND hist=0', array('chapter_id' => $id));
		echo ChaptersDraft::model()->deleteByPk($id);
		exit;
	}


	// Сортировка глав
	public function actionSortable()
	{
		$ar = $_POST['list'];
		$i  = 1;
		if (count($ar))
		{
			foreach ($ar as $id)
			{
				if ($id > 0)
				{
					ChaptersDraft::model()->updateByPk($id, array('order_num' => $i));
					$i++;
				}
			}
		}
	}
}