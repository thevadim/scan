<?php
class CommentsController extends Controller
{
	public $layout = 'index';

	public function actionIndex()
	{
		$this->setPageTitle('Комментарии');
		$criteria = new CDbCriteria();

		$criteria->select = 't.*, Profile.*';
		$criteria->order  = 't.date_publish DESC';

		$id          = Yii::app()->request->getParam('id', null);
		$user_id     = Yii::app()->request->getParam('user_id', null);
		$user_name   = Yii::app()->request->getParam('user_name', null);
		$entity_id   = Yii::app()->request->getParam('entity_id', null);
		$entity_name = Yii::app()->request->getParam('entity_name', null);

		if ($id)
		{
			$criteria->addCondition('t.id = ' . (int)$id);
		}

		if ($user_id)
		{
			$criteria->addCondition('t.user_id = ' . urldecode($user_id));
		}

		if ($user_name)
		{
			$criteria->addSearchCondition('t.user_name', urldecode($user_name));
		}

		if ($entity_id)
		{
			$criteria->addSearchCondition('t.entity_id', urldecode($entity_id));
		}

		if ($entity_name)
		{
			$criteria->addSearchCondition('t.entity_name', $entity_name);
		}


		$count = Comments::model()->count($criteria);

		$pages           = new CPagination($count);
		$pages->pageSize = 50;
		$pages->applyLimit($criteria);

		$cache_id = $pages->getCurrentPage();

		#$comments=Yii::app()->cache->get($cache_id);		

		#if($comments===false)
		{
			#var_dump('no cache');
			$comments = Comments::model()->with(array('Profile' => array('select' => 'status')))->findAll($criteria);
			#Yii::app()->cache->set($cache_id, $comments, 60);
		}

		$this->render('index', array('comments' => $comments, 'pages' => $pages));
	}

	// Для заданого id ставит или снимает active + меняет счетчик у статьи
	public function actionSetCommentsActive($id)
	{
		$model = Comments::model()->findByPk($id);
		if ($model->active == 1)
		{
			$model->active = 0;
			$counter       = -1;
		}
		else
		{
			$model->active = 1;
			$counter       = 1;
		}
		
		if ($model->entity_name == 'Articles')
		{
			Articles::model()->updateCounters(array('comments_count' => $counter), "id = :id", array(':id' => $model['entity_id']));
		}
		elseif ($model->entity_name == 'Post')
		{
			Post::model()->updateCounters(array('comments_count' => $counter), "id = :id", array(':id' => $model['entity_id']));
		}
		$model->save(false, 'active');
		$this->clearCache($model->entity_name, $model->entity_id);
	}

	public function clearCache($entity_name, $entity_id)
	{
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::COMMENTS_LIST, array(), 'ENTITY_NAME_' . $entity_name . '_ENTITY_ID_' . $entity_id );
		Yii::app()->cache->delete($CACHE_KEY);
		return true;	
	}	


	// Снимаем активность у васх сообщений пользователя + меняет счетчик у статьи
	public function actionSetAllCommentsActive($user_id, $active = 0)
	{
		Comments::model()->updateAll(array('active' => $active), 'user_id=:user_id', array('user_id' => $user_id));
		Comments::UpdateCounts();
	}


	public function actionStoplist()
	{
		$words = Yii::app()->getRequest()->getPost('words');

		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			CommentsStoplist::model()->deleteAll();
			if (is_array($words))
			{
				foreach ($words as $word)
				{
					$word = trim($word);
					if ($word)
					{
						$model       = new CommentsStoplist;
						$model->word = $word;
						$model->save();
					}
				}
			}
		}

		$this->setPageTitle('Стоп-лист');
		$list = CommentsStoplist::model()->findAll();
		$this->render('stoplist', array('list' => $list));
	}
}