<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alexander Parkhomenko <771067@gmail.com>
 * Date: 21.02.13
 * Time: 12:32
 */
class AtlasMainController extends Controller
{
	public function init()
	{
		Yii::import('application.modules.atlas.models.*');
		Yii::import('application.modules.atlas.components.*');
		$this->setPageTitle('Атлас');
	}
}