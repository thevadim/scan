<?php
/**
 * View for Tree widget
 */
?>

<select name="<?= $name ?>" class="b-admin__select">

	<option value="1">Все</option>
    <? foreach($tree as $t): ?>
        <option value="<?= $t->id ?>" <?=(($t->id == $parent_id) ? 'selected' : '')?> ><?=str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $t->clevel-1)?><?=$t->name?></option>
    <? endforeach; ?>

</select>