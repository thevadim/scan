<?php
class Afisha extends KeywordsItem
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Story the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_afisha';
    }

    public function behaviors()
    {
        return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );
    }

    public function scopes()
    {
        return array(
            /*Убирать нельзя, иначе sitemap.xml не сгенерится */
            'visible'=>array(
            ),
            'active' => array(
            	'condition' => 't.active = 1 AND NOW() >= t.date_active_start',
            ),
            'indexed'=>array(
                'index'=>'id',
            ),
        );
    }

    // Выбор N последних
    public function recently($limit = 1)
    {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 't.date_active_start DESC',
            'limit' => $limit,
        ));

        return $this;
    }

    // Выбор Last call
    public function isLastCall(){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'type_id=1',
            )
        );

        return $this;
    }

    // Выбор афиши недели
    public function isWeekAfisha(){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'type_id=2',
            )
        );

        return $this;
    }

    // не специальные афиши
    public function notSpecial(){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'type_id>2',
            )
        );

        return $this;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('name, link, image', 'required'),
            array('active, type_id', 'numerical', 'integerOnly'=>true),
            array('name, link, image, seo_title, seo_keywords, seo_description', 'length', 'max'=>255),
            array('date_active_start', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
            //array('date_active_end', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
            array('id, name, active, type_id', 'safe', 'on'=>'search')
        );

		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'name' => 'Название',
            'link' => 'Ссылка',
            'active' => 'Активность',
            'type_id' => 'Тип афиши',
            'date_active_start' => 'Дата начала активности',
            'date_active_end' => 'Дата окончания активности'
        );
		
		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
    }

    protected function afterValidate()
    {
        parent::afterValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
	
	public function getAfishatypesList()
	{
			$afishasids = array();
			$afishasids[''] = '';
			$afishas = Afisha::Model()->findAll();
			foreach($afishas as $afisha) {
				$afishasids[$afisha->id] = $afisha->name;
			}

			return $afishasids;
	}

    /**
     * @return string - вернуть ссылку на страницу этого типа
     */
    public function url()
    {
        return "/afisha/{$this->code}/";
    }

    private function resizeTo($photo, $x, $y)
	 {
        $photo->image_resize = true;
        $photo->image_ratio = true;
        $photo->image_x = $x;
        $photo->image_y = $y;
        $photo->jpeg_quality = 90;
        $photo->file_new_name_body = $this->id;
		  $photo->process(Yii::getPathOfAlias('webroot') . '/media/afisha/'.$x.'x'.$y);
    }

    protected function afterSave()
    {
        if( !empty($this->image) )
        {
            $image = Yii::app()->imagemod->load(Yii::getPathOfAlias('webroot') . $this->image);
            if(!file_exists(Yii::getPathOfAlias('webroot') . $this->image)){
                $image->process(Yii::getPathOfAlias('webroot') . $this->image);
            }

            $this->resizeTo($image, 120, 120);
        }

        parent::afterSave();
    }

	public function img($size = 'original')
	{
		if(0 < strlen($this->image))
		{
			$ext = substr($this->image, strrpos($this->image, '.') + 1);
			if('original' == $size)
			{
				return $this->image;
			}
			else
			{
				return "/media/afisha/120x120/" . $this->id . '.' . $ext;
			}
		}
		return false;
	}
}
?>