<?php

class ArticlesToRegions extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ArticlesToTags the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_articles_to_regions';
    }

    public function behaviors()
    {
        return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('article_id, region_id', 'required'),
            array('article_id, region_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, article_id, region_id', 'safe', 'on'=>'search'),
        );
    }

    /*Именованный СКОУП: Выбрать статьи определённого региона*/
    public function regionIs($region_id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'join' => 'INNER JOIN bg_articles a ON a.id=t.article_id',
            'condition' => 't.region_id = :region_id AND a.active=1',
			'params' => array(':region_id' => $region_id),
        ));

        return $this;
    }


	/*Именованный СКОУП: Выбрать регионы определённой статьи*/
	public function articleIs($article_id)
	{
		return Yii::app()->db->createCommand()
			->select('bg_regions.*')
			->from('bg_articles_to_regions t')
			->join('bg_regions', 'bg_regions.id = t.region_id')
			->where('article_id=:id', array(':id' => $article_id))
			->queryAll();
	}


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'articles'  => array(self::BELONGS_TO, 'Articles', array('article_id' => 'id'), ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'article_id' => 'Article',
            'region_id' => 'Region',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('article_id',$this->article_id);
        $criteria->compare('region_id',$this->region_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

 
}
?>