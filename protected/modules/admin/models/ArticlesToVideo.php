<?php

/**
 * This is the model class for table "articles_to_video".
 *
 * The followings are the available columns in table 'articles_to_video':
 * @property integer $id
 * @property integer $article_id
 * @property integer $video_id
 *
 * The followings are the available model relations:
 * @property Video $video
 * @property Articles $article
 */
class ArticlesToVideo extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return ArticlesToVideo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_articles_to_video';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('article_id, video_id', 'required'),
            array('article_id, video_id, sort', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, article_id, video_id, sort', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
           # 'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
			#'article' => array(self::BELONGS_TO, 'Articles', 'article_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'article_id' => 'Article',
            'video_id' => 'Video',
			'sort' => 'Sort',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('article_id',$this->article_id);
        $criteria->compare('video_id',$this->video_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
?>