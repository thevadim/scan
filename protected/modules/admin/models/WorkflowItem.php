<?php
class WorkflowItem extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_workflow_lock';
	}

	public function rules()
	{
		return array(
			array('article_id, user_id, lock_date', 'required'),
			array('article_id, user_id', 'numerical', 'integerOnly'=>true),
			array('id, article_id, user_id, lock_date', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'article_id' => 'Article ID',
			'user_id' => 'User ID',
			'lock_date' => 'Lock date',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('article_id',$this->article_id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeSave()
	{
		return parent::beforeSave();
	}
}