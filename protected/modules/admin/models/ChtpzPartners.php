<?php
class ChtpzPartners extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chtpz_partners';
	}



	public function relations()
	{
		return array(
			#'theme'              => array(self::BELONGS_TO, 'ChtpzThemes', 'theme_id'),

		);
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			#array('name', 'required'),
            array('top_article_id, block1_url, block2_url, block3_url, block1_pic, block2_pic, block3_pic', 'safe'),
			
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}

	public function getRandomBlock3Pic()
	{
		$block3_pics = explode("\r\n", $this->block3_pic);
		$block3_pics = array_filter($block3_pics);

		if(count($block3_pics) > 0)
		{
			shuffle($block3_pics);
			return $block3_pics[0];
		}

		return '/media/upload/files/280x222.jpg';
		
	}






}