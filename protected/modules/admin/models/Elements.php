<?php
class Elements extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_elements';
	}

	public function scopes()
	{
		return array(
			'visible' => array(),
			'indexed' => array(
				'index' => 'id',
			),
		);
	}

	/**
	 * Скоуп возвращает все элементы по заданому article_id с сортировкой по order_num ASC
	 */
	public function articlePk($article_id)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.article_id=:article_id',
				'order'     => 'order_num ASC',
				'params'    => array(':article_id' => $article_id),
			)
		);
		return $this;
	}

	/**
	 * Скоуп возвращает все элементы заданого типа
	 */
	public function typeIs($type)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.type=:type',
				'params'    => array(':type' => $type),
			)
		);
		return $this;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('seo_keywords, seo_description, fb_title, fb_description', 'safe', 'on'=>'search'),
			array('article_id', 'numerical', 'integerOnly' => true),
			array('chapter_id', 'numerical', 'integerOnly' => true),
			array('type', 'length', 'max' => 255),
			//array('content', 'length', 'max' => 100000),
			array('order_num', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{

	}
}