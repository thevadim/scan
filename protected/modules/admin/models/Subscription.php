<?

class Subscription extends CActiveRecord
{
	const BLOGGER = 1;
	const AUTHOR = 2;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_subscriptions';
	}


	public function rules()
	{
		$rules = array(
			array('watcher_id, author_id, watch_type', 'required'),
			array('watcher_id, author_id, watch_type', 'numerical', 'integerOnly' => true),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}




	public function relations()
	{
		return array(
			'Watcher' => array(self::BELONGS_TO, 'User', 'watcher_id'),
			'Author' => array(self::BELONGS_TO, 'User', 'author_id'),
		);
	}

}









