<?

class Blog extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_blogs';
	}


	public function rules()
	{
		$rules = array(
			array('name, code', 'required'),
			array('active, approved, is_premoderated, user_id', 'numerical', 'integerOnly' => true),
			array('name, cover', 'length', 'max' => 255),
			array('code', 'length', 'max' => 25),
			array('description', 'length', 'max' => 5000),
			array('code', 'unique'),
			array('date_create, date_update', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
			array('date_create', 'default', 'value' => new CDbExpression('NOW()')),
			array('is_premoderated', 'default', 'value' => 0),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	public function relations()
	{
		return array(
			'posts'  => array(self::HAS_MANY, 'Post', 'blog_id', 'order' => 'posts.date_create DESC'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}



	public function scopes()
	{
		return array(
			/*Убирать нельзя, иначе sitemap.xml не сгенерится */
			'visible'=>array(
				'condition' => 'active = 1',
			),
		);
	}



	public function attributeLabels()
	{
		$r = array(
			'id' => 'ID',
			'code' => 'Адрес',
			'active' => 'Опубликован',
			'is_premoderated' => 'Премодерация',
 			'name' => 'Заголовок блога',
			'date_create' => 'Дата создания',
			'date_update' => 'Дата обновления',
			'description' => 'Описание',
			'approved' => 'Выбор редакции',
		);

		if ($this->scenario == 'auth') {
			$r['email'] = 'Электронная почта';
		}

		return $r;
	}


	public static function getBloggersCount()
	{
		$sql = "
				SELECT count(*)
				FROM " . User::model()->tableName() . "
				WHERE id IN (
								SELECT user_id
								FROM bg_blogs b
								RIGHT JOIN (
									SELECT count(*) as posts, blog_id
									FROM " . Post::model()->tableName() . "
									WHERE active = 1
									GROUP BY blog_id
								) p ON (p.blog_id = b.id)
								WHERE active = 1
							)
				";

		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$bloggers = $command->queryScalar();

		return $bloggers;
	}


	public static function getBlogsCount()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'active = :active';
		$criteria->params = array(':active' => 1);
		$blogs = Blog::model()->count( $criteria );

		return $blogs;
	}


	public function url()
	{
		return Yii::app()->request->hostInfo . '/blogs/' . $this->code . '/';
	}
}





