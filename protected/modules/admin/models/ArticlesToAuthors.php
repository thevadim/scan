<?php

/**
 * This is the model class for table "bg_articles_to_authors".
 *
 * The followings are the available columns in table 'bg_articles_to_authors':
 * @property integer $id
 * @property integer $article_id
 * @property integer $author_id
 * @property string $type
 */
class ArticlesToAuthors extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ArticlesToAuthors the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_articles_to_authors';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('article_id, author_id, type', 'required'),
            array('article_id, author_id', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max'=>10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, article_id, author_id, type', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//Связь для получения авторов 
			//примерно так ArticlesToAuthors::model()->with('authors')->articleIdIs($id)->findAll();
			'authors'  => array(self::BELONGS_TO, 'Authors', array('author_id' => 'id'), ),
		);
	}
	
    /**
     * Скоуп для выбора всех авторов для заданой статьи, см relation authors.
     */
    public function articleIdIs($article_id)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'article_id = :article_id',
                'params' => array(':article_id' => $article_id),
            )
        );

        return $this;
    }	
	
    /**
     * Скоуп для выбора всех статей автора
     */
    public function authorIdIs($author_id)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'author_id = :author_id',
                'params' => array(':author_id' => $author_id),
            )
        );

        return $this;
    }	
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'article_id' => 'Article',
            'author_id' => 'Author',
            'type' => 'Type',
        );
    }

    /**
     * Возвращает является ли автором этого объекта $id
     * @param $id
     * @return bool
     */
    public function getAuthorIs($id)
    {
        return $this->author_id == $id;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('article_id',$this->article_id);
        $criteria->compare('author_id',$this->author_id);
        $criteria->compare('type',$this->type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 
?>