<?php
class Project extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'projects';
	}



	public function relations()
	{
		return array(
			#'tree'              => array(self::BELONGS_TO, 'Tree', 'tree_id'),

		);
	}

	public function scopes()
	{
		return array(
			'Indexed' => array(
				'index' => 'id',
			),
			'active'  => array(
				'condition' => 't.active = 1',
			),
		);
	}

	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'active' => 'Показывать на сайте',
			'code' => 'Название',
			'title' => 'Заголовок',
			'title_bg' => 'Заголовок на фоне',
			'show_regform' => 'Показывать форму регистрации',

		);

		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('code', 'required'),
            array('active, show_regform', 'safe'),
			array('code, title, title_bg', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}

	public function url()
	{
		return '/'.$this->code.'/';
	}



}