<?

class Ratings extends CActiveRecord
{

	//Сюда прописываем сущности, которые разрешеныы к рейтингованию пользователями
	private $entitiesToRate = array('Articles', 'Post', 'Users', 'Comments');

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_ratings';
	}


	public function rules()
	{
		$rules = array(
			array('user_id, entity_id, rating', 'numerical', 'integerOnly' => true),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	public function relations()
	{
		return array(
			'articles'  => array(self::HAS_MANY, 'Articles', 'entity_id', 'order' => 'articles.date_create DESC'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}



	public function attributeLabels()
	{
		$r = array(
			
		);

		return $r;
	}



	/**
	* Функция оценки любой разрешенной сущности ($this->entitiesToRate)
	* param string $entity_name
	* param int $entity_id
	* param int $rate +1 -1
	* @return array() of messages
	*/
	public function rateEntity($entity_name, $entity_id, $rate)
	{

		//Незарегеным нельзя голосовать
		if (Yii::app()->user->isGuest)
		{
			// throw new CHttpException(403);
			header('HTTP/1.0 403 Forbidden');
			return;
		}

		$user_id = Yii::app()->user->id;

		if(!in_array($entity_name, $this->entitiesToRate) )
			throw new CHttpException(404);

		if($rate != 1 && $rate != -1)
			throw new CHttpException(404);

		//Проверяем, не голосовал ли уже юзер
		/*$criteria = new CDbCriteria;
		$criteria->select = 'user_id';
		$criteria->condition = 'entity_name=:entity_name AND entity_id=:entity_id AND user_id=:user_id';
		$criteria->params = array(':entity_name' => $entity_name, ':entity_id' => $entity_id, ':user_id' => $user_id);
		$already_rate = Ratings::model()->find($criteria);
		*/
		//if(is_null($already_rate))

		//Удаляем голос пользователя (если есть)
		$condition = 'entity_name=:entity_name AND entity_id=:entity_id AND user_id=:user_id';
		$params = array(':entity_name' => $entity_name, ':entity_id' => $entity_id, ':user_id' => $user_id);
		Ratings::model()->deleteAll($condition, $params);

	
		//Проверяем существование заданоно айди сущности
		$entity = $entity_name::model()->findByPk($entity_id);			

		if(is_null($entity))
			throw new CHttpException(404);

		//Добавляем голос
		$rating = new Ratings();
		$rating->user_id = $user_id;
		$rating->entity_name = $entity_name;
		$rating->entity_id = $entity_id;
		$rating->rating = $rate;
		$rating->save();

		//Считаем новую сумму голосов
		$criteria = new CDbCriteria;
		$criteria->select = 'sum(rating) as rating';  // подходит только то имя поля, которое уже есть в модели
		$criteria->condition = 'entity_name=:entity_name AND entity_id=:entity_id';
		$criteria->params = array(':entity_name' => $entity_name, ':entity_id' => $entity_id);
		$new_rating = Ratings::model()->find($criteria)->getAttribute('rating'); 		

		//Обновляем значение рейтинга в таблице сущности
		$sql = 'UPDATE '.$entity_name::model()->tableName().' SET rating=:rating WHERE id=:id LIMIT 1';
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':rating', $new_rating);
		$command->bindParam(':id', $entity_id);
		$command->execute();


		$ret['status'] = 'success';
		$ret['rating'] = $new_rating;			
	
		

		//Сбрасываем кеш для голоса этого юзера по этой сущности
		Yii::app()->cache->delete( 'Ratings.getUserRateForEntity'.$entity_name.$entity_id.$user_id );

		//Сбрасываем кеш для getRating
		Yii::app()->cache->delete( 'Ratings.getRating'.$entity_name.$entity_id );
		

		return $ret;
	}

	/**
	* Получает полный рейтинг для заданой сущности по айди сущности
	* @return int рейтинг
	*/

	public function getRating($entity_name, $entity_id)
	{

		if(!in_array($entity_name, $this->entitiesToRate) )
			throw new CHttpException(500);	


		$CACHE_KEY = 'Ratings.getRating'.$entity_name.$entity_id;
		$rating = Yii::app()->cache->get($CACHE_KEY);

		if($rating === false)
		{
			$sql = "SELECT rating FROM ".$entity_name::model()->tableName()." WHERE id=:id LIMIT 1";
			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(':id', $entity_id);
			$result = $command->queryRow();

			if(isset($result['rating']) )
				$rating = $result['rating'];
			else
				$rating = 0;

			Yii::app()->cache->set($CACHE_KEY, $rating, 3600);
		}

		return $rating;
	}


	/**
	* Возвращает голос юзера для заданой сущности по айди сущности
	* param string $entity_name
	* param int $entity_id
	* param int $user_id - если не установлен, то берется текущий авторизованный.
	* @return int оценка юзера для конкретной сущности or NULL если не рейтинговал.  Если текущий юзер - Гость, то возвращает false.
	*/

	public function getUserRateForEntity($entity_name, $entity_id, $user_id = null)
	{
		if(Yii::app()->user->isGuest)
			return false;

		if(is_null($user_id))
			$user_id = Yii::app()->user->id;

		$CACHE_KEY = 'Ratings.getUserRateForEntity'.$entity_name.$entity_id.$user_id;
        $rating = Yii::app()->cache->get($CACHE_KEY);

        if($rating === false)
        {
			$sql = "SELECT rating FROM bg_ratings WHERE entity_name=:entity_name AND entity_id=:entity_id AND user_id=:user_id LIMIT 1";

			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(':entity_name', $entity_name);
			$command->bindParam(':entity_id', $entity_id);
			$command->bindParam(':user_id', $user_id);
			$result = $command->queryRow();	
			$rating = $result['rating'];

			Yii::app()->cache->set($CACHE_KEY, $rating, 3600);
		}

		return $rating;	
	}


	/**
	* Возвращает кол-во пользователей рейтинговавших сущность
	* @param string $entity_name
	* @param int $entity_id
	* @return object stdClass Object ( [total] => int [pros] => int [cons] => int )
	*/
	public function getEntityVoices($entity_name, $entity_id)
	{
		$CACHE_KEY = 'Ratings.getEntityVoices'.$entity_name.$entity_id;
        $voices = Yii::app()->cache->get($CACHE_KEY);

        if ($voices === false)
        {
			$sql = "
					SELECT count(*) total,
						SUM(CASE rating when 1 then 1 else 0 end) as pros,
						SUM(CASE rating when -1 then 1 else 0 end) as cons
					FROM bg_ratings
					WHERE entity_name = :entity_name AND entity_id = :entity_id
					";

			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(':entity_name', $entity_name);
			$command->bindParam(':entity_id', $entity_id);

			$result = $command->queryRow();
			$result = array_map('intval', $result);

			$voices = json_decode(json_encode($result), false);

			Yii::app()->cache->set($CACHE_KEY, $voices, 3600);
		}

		return $voices;
	}

}





