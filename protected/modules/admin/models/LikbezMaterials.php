<?php
class LikbezMaterials extends CActiveRecord
{

	#public $projects = array('main' => 'main', 'invest' => 'invest', 'vtb' => 'vtb', 'riw' => 'riw', );

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'likbez_materials';
	}



	public function relations()
	{
		return array(
			'tree'              => array(self::BELONGS_TO, 'Tree', 'tree_id'),
			'project'              => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}

	public function scopes()
	{
		return array(
			'Indexed' => array(
				'index' => 'id',
			),
			'active'  => array(
				'condition' => 't.active = 1',
			),
		);
	}

	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'active' => 'Показывать на сайте',
			'name' => 'Название',
			'lead' => 'Подзаголовок',
			'detail_text' => 'Текст',
			'date_active_start' => 'Дата на сайте',
			'pic' => 'Изображение',
			'pic_small' => 'Изображение для главной',
			'pic_desc' => 'Подпись для изображения',
			'tree_id' => 'Рубрика',
			'gallery_id' => 'Фотогалерея',
			'poll_id' => 'Опрос',
			'ad_link_text' => 'Текст рекламной ссылки',
			'ad_link_url' => 'Рекламная ссылка',
			'project_id' => 'Проект',
		);

		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name', 'required'),
            array('active, predetail_text, date_active_start, detail_text, lead, left_col_text, tree_id, gallery_id, poll_id, ad_link_text, ad_link_url', 'safe'),
			array('name, project_id, pic, pic_small, pic_desc', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}

	public function url()
	{
		return '/articles/'.$this->id.'/';
	}



}