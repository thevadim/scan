<?php
class LikbezRubrics extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'likbez_rubrics';
	}



	

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name', 'required'),
            array('detail_text, year, month, date_words, quote_text, quote_name, quote_desc', 'safe'),
			array('name', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}





}