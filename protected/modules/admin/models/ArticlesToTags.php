<?php

/**
 * This is the model class for table "articles_to_tags".
 *
 * The followings are the available columns in table 'articles_to_tags':
 * @property integer $id
 * @property integer $article_id
 * @property integer $tag_id
 */
class ArticlesToTags extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ArticlesToTags the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_articles_to_tags';
    }

    public function behaviors()
    {
        return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('article_id, tag_id', 'required'),
            array('article_id, tag_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, article_id, tag_id', 'safe', 'on'=>'search'),
        );
    }

    /*Именованный СКОУП: Выбрать статьи определённого тага*/
    public function tagIs($tag_id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'join' => 'INNER JOIN bg_articles a ON a.id=t.article_id',
            'condition' => 't.tag_id = :tag_id AND a.active=1',
			'params' => array(':tag_id' => $tag_id),
        ));

        return $this;
    }


	/*Именованный СКОУП: Выбрать теги определённой статьи*/
	public function article($article_id)
	{
		return Yii::app()->db->createCommand()
			->select('bg_tags.*')
			->from('bg_articles_to_tags t')
			->join('bg_tags', 'bg_tags.id = t.tag_id')
			->where('article_id=:id', array(':id' => $article_id))
			->queryAll();
	}

	/**
	 * Скоуп. Выбирает статьи с галкой "Гид"
	 */
	public function isGuide()
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'is_guide=1',
			)
		);

		return $this;
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'articles'  => array(self::BELONGS_TO, 'Articles', array('article_id' => 'id'), ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'article_id' => 'Article',
            'tag_id' => 'Tag',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('article_id',$this->article_id);
        $criteria->compare('tag_id',$this->tag_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Является ли данный объект тегом с уазанным id
     *
     * @param $tag_id - id тега
     * @return bool
     */
    public function getTagId($tag_id)
    {
        return $this->tag_id+0 == $tag_id+0;
    }
	
	public function articleIs($article_id)
	{
		return $this->article_id == $article_id;
	}
}
?>