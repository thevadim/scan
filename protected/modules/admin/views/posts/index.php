<h1>Посты</h1>
<br />
<br />

<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/posts/')), 'get'); ?>
<div class="well">
	<table class="b-search-params">
		<tr>
			<td class="b-search-params__option"><? echo CHtml::Label('ID', 'id'); ?></td>
			<td class="b-search-params__value"><? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><? echo CHtml::Label('Название', 'name'); ?></td>
			<td class="b-search-params__value"><? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><? echo CHtml::Label('Блог', 'blog_id'); ?></td>
			<td class="b-search-params__value">
				<?
				$blog_list = Blog::model()->findAll(array('index' => 'id'));
				$list[-1] = 'Неважно';
				foreach ($blog_list as $b)
				{
					$list[$b->id] = $b->name;
				}
				?>
				<? echo CHtml::dropDownList('blog_id', urldecode(Yii::app()->request->getParam('blog_id','')), $list); ?>
			</td>
		</tr>
		<tr>
			<td class="b-search-params__option"><? echo CHtml::Label('Опубликован', 'active'); ?></td>
			<td class="b-search-params__value">
				<? echo CHtml::dropDownList('active', Yii::app()->request->getParam('active',''), array(-1 => 'Неважно', 1 => 'Да', 0 => 'Нет')); ?>
			</td>
		</tr>
		<tr>
			<td class="b-search-params__action" colspan="2">
				<? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?>
				<a href="/admin/posts/" class="btn">Сброс</a>
			</td>
		</tr>
	</table>
</div>
<? echo CHtml::endForm(); ?>


<table class="table b-admin__articles-list" >
	<thead>
		<tr>
			<th style="text-align: center; width: 60px;">&nbsp;</th>  
			<th style="text-align: left;"><b>Название</b></th>
			<th style="text-align: left;"><b>Блог</b></th>
			<th style="text-align: left;"><b>Дата создания</b></th>
			<th style="text-align: left;">ID</th>
		</tr>
	</thead>
	<? foreach($posts as $p) : ?>
	<tr class="b-admin__articles-list__row">
		<td>
			<div class="b-admin__articles-list__icons">
				<a href="javascript:void(0)" class="article_toggle_active" data-id="<?=$p->id?>">
					<i class="icon-<?=(($p['active'] == 1) ? 'circle' : 'circle-blank' )?>"></i>
				</a>
				<?
				if ($p->approved)
				{
					?><i class="icon-certificate" title="Выбор редакции"></i><?
				}
				?>
			</div>
		</td>
		<td style="text-align: left;">
			<a href="/admin/posts/edit/?id=<?= $p->id ?>"><?= $p->name ?></a>
		</td>
		<td style="text-align: left;" class="b-admin__articles-list__icons">
			<?
			if ($p->blog_approved)
			{
				?><i class="icon-certificate" title="Выбор редакции"></i><?
			}
			?>
			<a href="/admin/blogs/edit/?id=<?= $p->blog->id ?>"><?= $p->blog->name ?></a>
		</td>
		<td style="text-align: left;">
			<?= $p->date_create ?>
		</td>
		<td style="text-align: left;">
			<?= $p->id?>
		</td>
	</tr>
	<? endforeach ?>
	
</table>

<?$this->widget('Pagination', array(
		'pages' => $pages,
))?>