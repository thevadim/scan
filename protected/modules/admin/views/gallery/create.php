<div class="gallery-form">
	<div class="page-header">
		<h1>Создание Галереи</h1>
	</div>

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'action' => '/admin/gallery/save',
		'id' => 'new-gallery-form',
		'enableClientValidation' => false,
		'enableAjaxValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => true,	
		),
		'htmlOptions' => array(
		    'class' => 'form-horizontal',
		),
	)); 
	?>
	<fieldset>
		<div class="control-group">
			<?= $form->labelEx($Galleries, 'name', array('class' => 'control-label gallery-name')) ?>
			<?= $form->textfield($Galleries, 'name') ?>
			<span class="help-inline"><?=$form->error($Galleries, 'name') ?></span>
		</div>	
	</fieldset>
	<?//=$form->errorSummary($Galleries, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

	<?php $this->endWidget(); ?>
	<h3>Загрузить изображения</h3>
	<br/>
	<?php $this->widget('ext.xupload.XUpload', array(
	        'url' => "/admin/gallery/uploadImages/",
	        'model' => $XUploadForm,
	        'attribute' => 'file',
	        'multiple' => true,
	));?>

	<div class="form-actions">
		<input class="btn btn-success submit" name="save" id="gallery-save" type="button" value="Сохранить">		
		<input class="btn btn-primary apply" name="apply" id="gallery-apply" type="button" value="Применить">		
	</div>
</div>