<a href="/admin/banners/edit/" class="btn btn-success">Создать баннер</a>

<br/><br/>

<table class="table">
    <thead>
        <tr>
            <th style="text-align: center;"><b>#</b></th>
            <th style="text-align: center;"><b>Активность</b></hd>
            <th><b>Название</b></th>
            <th><b>Код</b></th>
        </tr>
    </thead>

    <? foreach($banners as $banner) : ?>
    <tr>
        <td style="text-align: center;">
            <?=$banner['id']?>
        </td>
        <td style="text-align: center;">
            <? if($banner->active) :?> Да <? else : ?> Нет <? endif ?>
        </td>
        <td>
            <?php echo CHtml::link(CHtml::encode($banner['name']), "/admin/banners/edit/id/{$banner->id}", array());?>
        </td>
        <td>
            <?php echo CHtml::encode($banner['code']);?>
        </td>
    </tr>
    <? endforeach ?>

</table>

<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>
