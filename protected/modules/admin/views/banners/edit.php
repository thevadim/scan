<h1><?
if( empty($model->id) )
{
	?>Создание баннера<?
}
else
{
	?>Баннер #<?=$model->id?>, <?=$model->name?><?
}
?>
</h1>

<div class="span10">
	<div class="tab-content">
		<div class="tab-pane active" id="content">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action' => '/admin/banners/edit' . (!empty($model->id) ? "/id/{$model->id}/" : '/'),
			'id'=>'banner-form',
			//'enableClientValidation'=>true,
			//'enableAjaxValidation'=>true,
			'clientOptions'=>array(
				//'validateOnSubmit'=>true,
			),
			'htmlOptions' => array(
                'class' => 'form-horizontal',
			),
		)); ?>

		<p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->checkBox($model, 'active') ?>
							<span class="help-inline"><?= $form->error($model, 'active') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<div class="input-append">
								<?echo $form->textField($model, 'code', array());?>
							</div>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'html_code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textArea($model, 'html_code', array('class' => 'span8', 'style'=>'height:300px;')) ?>
							<span class="help-inline"><?= $form->error($model, 'html_code') ?></span>
						</div>
					</div>
				</div>
			</div>


		</fieldset>

		<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

		<div class="form-actions">
			<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
			<?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)) ?>
			<?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
			<div style="float:right;">
				<? echo CHtml::ajaxButton(
					'Удалить',
					'/admin/banners/delete',
					array(
						'data' => "id={$model->id}",
						'beforeSend' => 'function(){}',
						'complete' => 'function(){}',
						'success' => 'function(){alert("Баннер успешно удалён!"); window.location.href="/admin/banners/";}',
						'error' => 'function(){alert("Во время удаления баннера произошла непредвиденная ошибка!");}',
					),
					array(
						'class' => 'btn btn-danger',
						'id'=>'delete_btn',
						'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот баннер?',
					)
				); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
	Yii::app()->getClientScript()->registerScript('cancel_btn_banners', "
		$('#cancel_btn').click( function(e) {
			window.location.href = '/admin/banners/';
		});
	", CClientScript::POS_READY);
?>