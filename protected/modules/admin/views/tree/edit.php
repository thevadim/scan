<?
if($action == 'create')
{
	?><h1>Создание страницы</h1><?
}
else
{
	?>
	<h1>Редактирование страницы</h1>

	<ul class="breadcrumb">
		<li>
			<a href="/admin/">Главная</a> <span class="divider">/</span>
		</li>
		<li>
			<a href="/admin/tree/">Дерево сайта</a> <span class="divider">/</span>
		</li>
		<?
		foreach($descendants as $d)
		{
			?>
			<li>
				<a href="/admin/tree/edit/?id=<?=$d->id?>"><?=$d->name?></a> <span class="divider">/</span>
			</li>
			<?
		}
		?>
		<li class="active"><?=$model->name?></li>
	</ul>
	<?
}
?>


<div class="span10">
	<div class="tab-content">
		<div class="tab-pane active" id="content">

		<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tree-form',
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,	
			),
			'htmlOptions' => array(
			    'class' => 'form-horizontal',
			),
		)); 
		?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'parent_id', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<select id="node_parent_id" name="Tree[parent_id]">
							<?
							foreach($tree as $t)
							{
							?>
								<option value="<?=$t->id?>" <?=(($t->id == $parent_id) ? 'selected' : '')?> ><?=str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $t->clevel-1)?><?=$t->name?></option>
							<?
							}
							?>
							</select>
							<span class="help-inline"><?= $form->error($model, 'node_parent_id') ?></span>
						</div>
					</div>
				</div>
			</div>

            <div class="control-group">
                <?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
                <div class="controls">
                    <div class="row">
                        <div class="span8">
                            <?= $form->checkBox($model, 'active') ?>
                            <span class="help-inline"><?= $form->error($model, 'active') ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

            <?/*if( $model->isDescendantOf(Tree::model()->findByPk(Tree::TELESHOW_NODE_ID)) ):?>
                <div class="control-group">
                    <?= $form->labelEx($model, 'eng_name', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <div class="row">
                            <div class="span8">
                                <?= $form->textField($model, 'eng_name', array('class' => 'span8')) ?>
                                <span class="help-inline"><?= $form->error($model, 'eng_name') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?endif*/?>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<div class="input-append"><?
								echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span7'));
								$cookie_var = Yii::app()->request->cookies['transliterate_lock_state'];
								?><button data-action="trigger-lock" data-sync-to="slug" class="btn icon" onclick="dynamicClick($(this)); return false;"><?= isset($cookie_var->value[ Yii::app()->request->requestUri ]) ? $cookie_var->value[ Yii::app()->request->requestUri ] : 'w' ?></button>
							</div>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_title', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_keywords', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_keywords', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_keywords') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_description', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textArea($model, 'seo_description', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_description') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'preview_img', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textField($model, 'preview_img', array('class' => 'span6'))?>
							<span class="help-inline"><?php echo $form->error($model, 'preview_img'); ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'detail_img', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textField($model, 'detail_img', array('class' => 'span6'))?>
							<span class="help-inline"><?php echo $form->error($model, 'detail_img'); ?></span>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				$(function() {
					ckfinderManager.create('<?=get_class($model)?>_preview_img');
					ckfinderManager.create('<?=get_class($model)?>_detail_img');
				});
			</script>


			<div class="control-group">
				<?= $form->labelEx($model, 'preview_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">			
							<?=$form->textArea($model,'preview_text');?>		
							<script>
								$(document).ready(function(){
									wysiwygManager.createEditor('Tree_preview_text', {config: 'config.js'});
								});
							</script>								
							
							<span class="help-inline"><?= $form->error($model, 'preview_text') ?></span>
						</div>
					</div>
				</div>
			</div>

			<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

			<div class="form-actions">
				<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
				<?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)) ?>
				<?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
				<div style="float:right;">
					<?if(!in_array($model->id, $this->deny_to_delete)){?>
					<? echo CHtml::ajaxButton(
						'Удалить',
						'/admin/tree/deletearticle',
						array(
							'data' => "id={$model->id}",
							'beforeSend' => 'function(){}',
							'complete' => 'function(){}',
							'success' => 'function(){alert("Статья успешно удалена!"); window.location.href="/admin/tree/";}',
							'error' => 'function(){alert("Во время удаления статьи произошла непредвиденная ошибка!");}',
						),
						array(
							'class' => 'btn btn-danger',
							'id'=>'delete_btn',
							'confirm'=>'Вы уверены, что хотите безвозвратно удалить эту статью?',
						)
					); ?>

					<?}?>
				</div>
			</div>
		</fieldset>

		<?php $this->endWidget(); ?>

		<?
		if(isset($articles))
		{
			?>
			<h2>Последние статьи</h2>
			<?
			foreach($articles as $a)
			{
			?>
				<a href="/admin/articles/edit/?id=<?=$a->id?>"><?=$a->name?></a><br />
			<?
			}
			?>
			<?
		}
		?>
	</div>
</div>


<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_votes', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/tree/";
    });
', CClientScript::POS_READY);
?>

