<div class="item_container" style="margin-bottom: 20px;">
	<table style="background-color: #eee; border-collapse: separate; border-spacing: 4px;">
		<tr>
			<td></td>
			<td><? echo CHtml::activeTextField($item,'name', array('name' => "PollItems[{$item->id}][name]", 'class' => 'span8')); ?></td>
			<td rowspan="4"><a href="" class="btn btn-small" onclick="$(this).closest('div.item_container').remove(); return false;"><i class="icon-minus"></i> Удалить</a></td>
		</tr>
		<tr>
			<td>Изображение</td>
			<td>
				<?echo CHtml::activeTextField($item, 'img', array('name' => "PollItems[{$item->id}][img]", 'class' => 'span6'))?>
				<script type="text/javascript">
					$(function() {
						ckfinderManager.create('PollItems_<?=$item->id?>_img');
					});
				</script>
			</td>
		</tr>
		<tr>
			<td>Ссылка</td>
			<td><? echo CHtml::activeTextField($item,'link', array('name' => "PollItems[{$item->id}][link]", 'class' => 'span8')); ?></td>
		</tr>
		<tr>
			<td>Описание</td>
			<td><? echo CHtml::activeTextArea($item,'description', array('name' => "PollItems[{$item->id}][description]", 'class' => 'span8', 'rows' => 5)); ?></td>
		</tr>
	</table>
</div>