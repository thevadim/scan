<h1>Настройки сайта </h1>


<?
$form = $this->beginWidget('CActiveForm', array(
	'action'                 => !empty($model->id) ? "?id={$model->id}" : '',
	'id'                     => 'articles-form',
	'enableClientValidation' => true,
	'enableAjaxValidation'   => true,
	'clientOptions'          => array(
		'validateOnSubmit' => true,
	),
	'htmlOptions'            => array(
		'class' => 'form-horizontal',
	),
));
?>

<?
foreach($settings as $s)
{
	?>
		<div class="control-group">
			<label class="control-label"><?=$s->name?></label>
			<div class="controls">
				<div class="input-append span10">
					<?= CHtml::textField("settings[{$s->code}]", $s->value, array('class' => 'span8')) ?>
					
				</div>
				
			</div>
		</div>
	<?
}
?>

<?= CHtml::submitButton('Сохранить')?>

<?
$this->endWidget(); 
?>