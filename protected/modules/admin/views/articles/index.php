<a href="/admin/articles/create/" class="btn btn-success">Создать статью</a>
<br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/articles/')), 'get'); ?>


<div class="well">
  <table class="b-search-params">
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('ID', 'id'); ?></td>
        <td class="b-search-params__value"><? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?></td>
      </tr>
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Категория', 'category_id'); ?></td>
        <td class="b-search-params__value new-height-select">
            <? $this->widget(
                'TreeWidget',
                array(
                    'name' => 'parent_id',
                    'parent_id' => Yii::app()->request->getParam('parent_id'),
                    'parent_scope' => 1
                )
            ); ?>
        </td>
      </tr>
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Название', 'name'); ?></td>
        <td class="b-search-params__value"><? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?></td>
      </tr>
     <tr>
       <td class="b-search-params__value" colspan="2">
        <?/*
        <label for="is_best" class="checkbox inline">
          <? echo CHtml::CheckBox('is_best', Yii::app()->request->getParam('is_best','')); ?> Лучшая статья
        </label>

        <label for="is_guide" class="checkbox inline">
         <? echo CHtml::CheckBox('is_guide', Yii::app()->request->getParam('is_guide','')); ?> Гид
        </label>  
        */?>      

        <label for="noactive" class="checkbox inline">
         <? echo CHtml::CheckBox('noactive', Yii::app()->request->getParam('noactive', '')); ?> Не активна
        </label>  
      </td>
    </tr>
    <tr>
      <td class="b-search-params__action" colspan="2"><? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?></td>
    </tr>
  </table>
</div>

<? echo CHtml::endForm(); ?>

<table class="table b-admin__articles-list" >
  <thead>
    <tr>
      <th style="text-align: center; width: 20px;">&nbsp;</th>
      <th style="text-align: center; width: 100px;"><!-- <b>Картинка</b> --></th>     
      <th style="text-align: left;"><b>Название</b></th>
      <th style="text-align: left; width: 150px;"><b>Раздел</b></th>
      <th style="text-align: left; width: 100px;"><i class="icon-eye-open"></i></th>
      <th style="text-align: left; width: 100px;"><b>Дата на сайте</b></th>
      <th style="text-align: left; width: 100px;"><b>Изменено</b></th>
      <th style="text-align: left;">Перейти</th>
    </tr>
  </thead>
  <? foreach($news as $n) : ?>

  <?
  //Если не прочитано корректором, помечаем
  $proof_class = '';
  //if($n->proof_tested == 0){$proof_class = 'warning';}
  ?>
  <tr class="b-admin__articles-list__row <?=$proof_class?>">
    <td>
      <div class="b-admin__articles-list__icons">
        <a href="javascript:void(0)" class="article_toggle_active" data-id="<?=$n->id?>">
          <i class="icon-<?=(($n->active == 1) ? 'circle' : 'circle-blank' )?>"></i>
          <!--
          <img src="/static/css/admin/images/<?=(($n['active'] == 1) ? 'accept.png' : 'delete.png' )?>" /> --></a>
        <?/*<a class="article_toggle_active btn btn-<?=(($n['active'] == 1) ? 'success' : 'danger')?> btn-mini" data-id="<?=$n->id?>">&nbsp;&nbsp;</a><br />*/?>
        <?if($n->show_main_announce == 1){?><i class="icon-bookmark" title="Главная на морде"></i> <!--<img src="/static/css/admin/images/flag_blue.png" title="Главная на морде"/>--><?}?>
        <?if($n->show_main_announce_section == 1){?><i class="icon-bookmark-empty" title="Главная раздела"></i><!--<img src="/static/css/admin/images/flag_yellow.png" title="Главная раздела" />--><?}?>
      </div>
    </td>
    <td style="text-align: center;"><?if(strlen($n->preview_img) > 0){?><img src="<?=ThumbsMaster::getThumb($n->preview_img, ThumbsMaster::$settings['155_auto'])?>" width="80"/><?}?></td>
    <td style="text-align: left;">
      <a href="/admin/articles/edit/?id=<?=$n['id']?>">
        <?=((strlen($n->name) > 0) ? CHtml::encode($n->name) : 'Статья без названия')?>
      </a>

      <div>
        <?
        $all_authors = array();
        foreach ($n->authors as $author_type => $author) {
          $all_authors[] = $author->fullname();
        }

        echo implode(', ', $all_authors);
        ?>
      </div>

      <? if( !empty($n->lock) ) : ?>
          <? if($n->lock->user_id != Yii::App()->user->id) : ?>
              <span class="label label-important">
                  Пользователь <?=CHtml::encode( ProfileHelper::getFIO($n->lock->user_id) );?> (<?=$n->lock->user_id;?>)
                  в <?=$n->lock->lock_date?>
              </span>
              &nbsp;
          <? else : ?>
              <span class="label label-warning">Заблокировано Вами в <?=$n->lock->lock_date?></span>
          <? endif ?>
      <? endif ?>      
    </td>
    <td style="text-align: left;">
      <?=((isset($n->tree)) ? CHtml::encode($n->tree->name) : '')?>
      <?= ($n->is_guide == 1) ? '<span class="label label-success">гид</span>' : ''?>
      <?= ($n->has_video == 1) ? '<img src="/static/css/admin/images/video.png" />' : ''?>
      <?= ($n->has_gallery == 1) ? '<img src="/static/css/admin/images/camera.png" />' : ''?>
      
    </td>

        <td style="text-align: left;">
            <?=$stat[$n->id]->views?>
        </td>
      <td style="text-align: left;">
      <?=$n->date_publish?>
     </td>        
     <td style="text-align: left;">
      <?=$n->x_timestamp?>
     </td>
    <td style="text-align: left;">
      <a href="<?=$n->url()?>" target="_blank">ссылка</a>
    </td>
  </tr>
  <? endforeach ?>
  
</table>

<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>