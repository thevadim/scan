<h1><?
    if( empty($model->id) )
    {
        ?>Создание лекции<?
    }
    else
    {
        ?>Сообщение #<?=$model->id?><?
    }
    ?>
</h1>

<div class="span10">
<div class="tab-content">
<div class="tab-pane active" id="content">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action' => '/admin/twitter/edit' . (!empty($model->id) ? "/id/{$model->id}/" : '/'),
    'id'=>'twitter-form',
    //'enableClientValidation'=>true,
    //'enableAjaxValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    ),
)); ?>

<p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

<fieldset>

    <div class="control-group">
        <?= $form->labelEx($model, 'message', array('class' => 'control-label')) ?>
        <div class="controls">
            <div class="row">
                <div class="span8">
                    <?= $form->textArea($model, 'message', array('class' => 'span8', 'id'=>'twitterMsg')) ?>
                    <span class="help-inline"><?= $form->error($model, 'message') ?></span>
                </div>
            </div>
            <div style="padding-left: 10px;">
                <span id="twitterMsgRestrictionTitle"></span>
                <span id="twitterMsgRestriction" style="color: red; font-weight: bold;"></span>
            </div>
        </div>
    </div>

</fieldset>

<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

<div class="form-actions">
    <?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)) ?>
    <?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/twitter/delete',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Сообщение успешно удалено!"); window.location.href="/admin/twitter/";}',
            'error' => 'function(){alert("Во время удаления сообщения произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить это сообщение?',
        )
    ); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
</div>
</div>
</div>

<?php
	Yii::app()->getClientScript()->registerScript('cancel_btn_lectures', "
		$('#cancel_btn').click( function(e) {
			window.location.href = '/admin/twitter/';
		});

		$('#twitterMsg').bind('keyup', function() {
		    var left_chars = 140 - parseInt( $('#twitterMsg').val().length );
		    $('#twitterMsgRestrictionTitle').text('Осталось символов: ');
		    $('#twitterMsgRestriction').text( left_chars );
		});

	", CClientScript::POS_READY);
?>