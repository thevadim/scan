<div class="b-admin__breadcrumbs">
		<a href="/admin/comments/" class="b-admin__breadcrumbs__link">Комментарии</a> /
</div>
<h1>Стоп-лист для комментариев</h1>
<span class="btn btn-primary" id="addword">Добавить слово</span>

<form action="" method="post">
	<table id="tstoplist">
		<?php
		foreach ($list as $val)
		{
			?>
			<tr>
				<td style="padding-bottom: 10px;">
					<input type="text" name="words[]" value="<?=$val->word?>"/>
					<span class="btn btn-mini btn-danger"><i class="icon-minus-sign"></i></span>
				</td>
			</tr>
		<? }?>
	</table>
	<input type="submit" class="btn btn-success" value="Сохранить"/>
</form>

<style type="text/css">
	span.btn { cursor:pointer; }
	#addword { margin-bottom:20px; }
</style>
<script type="text/javascript">
	$(function() {
		$("#addword").click(function() {

			$("#tstoplist").append('<tr><td><input name="words[]"/> <span class="btn btn-mini btn-danger deleteword">Удалить</span></td></tr>');
		});

		$(".deleteword").live("click", function() {
			$(this).parent().parent().remove();
		});
	});
</script>