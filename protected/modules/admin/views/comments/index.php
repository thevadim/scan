<script>
	$(document).ready(function() {

		$( document ).ajaxStart(function() {
			$( "#ajaxLoading" ).show();
		});

		$( document ).ajaxStop(function() {
			$( "#ajaxLoading" ).hide();
		});

		$('.set_comments_active').click(function() {
			$.ajax({
						 url    : "/admin/comments/setcommentsactive/",
						 context: this,
						 data   : {id: $(this).attr('data-id'), active: $(this).attr('data-active') },
						 cache  : false,
						 success: function(data) {
							 if ($(this).attr('data-active') == 1)
							 {
								 $(this).html('Активировать');
								 $(this).attr('data-active', 0);
							 }
							 else
							 {
								 $(this).html('Деактивировать');
								 $(this).attr('data-active', 1);
							 }
						 }
					 });
		});

		$('.set_allcomments_active').click(function() {
			$.ajax({
						 url    : "/admin/comments/setallcommentsactive/",
						 context: this,
						 data   : {user_id: $(this).attr('data-id'), active: $(this).attr('data-active')},
						 cache  : false,
						 success: function(data) {
							 $('.set_user_status[data-id="' + $(this).attr('data-id') + '"]').parent().parent().find(".set_comments_active").html("Активировать").attr("data-active", 0);
						 }
					 });
		});

		$('.set_user_status').click(function() {
			$.ajax({
						 url    : "/admin/users/setuseractive/",
						 context: this,
						 data   : {id: $(this).attr('data-id')},
						 cache  : false,
						 success: function(data) {
							 var list_comments_for_users_obj = $('.set_user_status[data-id="' + $(this).attr('data-id') + '"]');
							 if ($(this).attr('data-status') == 1)
							 {
								 list_comments_for_users_obj.html('Разбанить');
								 $(this).attr('data-status', -1);
							 }
							 else
							 {
								 list_comments_for_users_obj.html('Забанить');
								 $(this).attr('data-status', 1);
							 }
						 }
					 });
		});
	});
</script>

<div id="ajaxLoading" style="display:none; background-color: #0049cc; color:#fff; padding: 5px; position: fixed; bottom: 20px;">Идет обработка запроса...</div>

<h1>Комментарии</h1>
<p><a class="btn" href="/admin/comments/stoplist/">Стоп-лист для комментариев</a></p>


<?= CHtml::beginForm('/admin/comments/', 'get') ?>

<div class="well">
	<table class="b-search-params">
		<tr>
			<td class="b-search-params__option"><?= CHtml::Label('ID', 'id') ?></td>
			<td class="b-search-params__value"><?= CHtml::textField('id', Yii::app()->request->getParam('id')) ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><?= CHtml::Label('Article ID', 'entity_id') ?></td>
			<td class="b-search-params__value"><?= CHtml::textField('entity_id', urldecode(Yii::app()->request->getParam('entity_id'))) ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><?= CHtml::Label('User ID', 'user_id') ?></td>
			<td class="b-search-params__value"><?= CHtml::textField('user_id', Yii::app()->request->getParam('user_id')) ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><?= CHtml::Label('Имя пользователя', 'user_name') ?></td>
			<td class="b-search-params__value"><?= CHtml::textField('user_name', urldecode(Yii::app()->request->getParam('user_name'))) ?></td>
		</tr>
		<tr>
			<td class="b-search-params__option"><?= CHtml::Label('Раздел', 'entity') ?></td>
			<td class="b-search-params__value">
				<?php
				$data = array(
					''         => 'Все',
					'Articles' => 'Статьи',
					//'Blogs'    => 'Блоги',
					//'Atlas'    => 'Атлас'
				);
				echo CHtml::DropDownList('entity_name', isset($_GET['entity_name']) ? $_GET['entity_name'] : '', $data, array('class' => 'b-admin__select'));
				?>

			</td>
		</tr>
		<tr>
			<td
			<td class="b-search-params__action" colspan="2"><?= CHtml::submitButton('Искать', array('name' => '', 'class' => 'btn btn-primary')) ?></td>
		</tr>
	</table>
</div>
<? echo CHtml::endForm(); ?>


<?
foreach ($comments as $cm)
{
	
	?>
	<table class="table table-bordered table-stripped table-condensed" <? if ($cm->active == 0){?> style="background: #fee6e5;" <?}?>>
		<tr style="background-color: #eee">
			<td>
				[<a href="/admin/users/edit/id/<?= $cm->user_id ?>/"><?= $cm->user_id ?></a>]&nbsp;<b><?= $cm->user_name ?></b>
				<?= $cm->date_publish ?> <? /*if(isset($cm->Profile)){?><?=$cm->Profile->username?> <?=$cm->Profile->firstname?> <?=$cm->Profile->lastname?><?}*/ ?>
				<a href="javascript:void(0)" class="set_user_status" data-id="<?= $cm->user_id ?>" data-status="<?= $cm->Profile->status ?>"><?= (($cm->Profile->status == 1) ? 'Забанить' : 'Разбанить') ?></a>
						
				<a href="javascript:void(0)" class="set_allcomments_active" data-id="<?= $cm->user_id ?>" data-active="0" style="display:inline-block; float:right;">Деактивировать все</a>
				<a href="javascript:void(0)" class="set_allcomments_active" data-id="<?= $cm->user_id ?>" data-active="1" style="display:inline-block; float:right; margin: 0px 10px;">Активировать все</a>
			</td>
		</tr>
		<tr>
			<td>
				<div data-comment_id="<?=$cm->id?>">
					<p class="commtext"><?= $cm->message ?></p>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<a href="javascript:void(0)" class="set_comments_active" data-id="<?= $cm->id ?>" data-active="<?= $cm->active ?>"><?= (($cm->active == 1) ? 'Деактивировать' : 'Активировать') ?></a>
				<a href="javascript:void(0)" onclick="comments.edit($(this))" data-comment_id="<?=$cm->id?>" style="margin-left: 20px;">Редактировать</a>
							
				<a href="<?=$cm->getLink()?>" style="display:inline-block; float:right;"><?=$cm->getEntityField('name')?></a>
			</td>
		</tr>
	</table>
<?
}
?>

<?$this->widget('Pagination', array(
	'pages' => $pages,
))?>